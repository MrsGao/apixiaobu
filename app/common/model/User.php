<?php


namespace app\common\model;


use think\console\Command;
use think\Model;

class User extends Model
{
    /**
     *获取用户名
     * @param $user 输入账号
     * @param $passwor 输入密码
     * @return array|Model|\think\response\Json|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */

    public static function getAdminLogin($user,$passwor){
        if (empty($user) || empty($passwor)){
            return show(config('status.error'),'传值不能为空');
        }else{

          $user =   User::where('username',trim($user))->where('password',trim(md5($passwor)))->find();

          return  $user;
        }

    }

    /**
     * 更新数据id
     * @param $id
     * @param $data
     * @return User|false
     */

    public function updateById($id,$data){
        if (empty($id) || empty($data) || !is_array($data)){
            return false;}
       $users =  User::where('id',$id)->update($data);
       return  $users;

    }
}