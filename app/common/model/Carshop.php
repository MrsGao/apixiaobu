<?php


namespace app\common\model;


use think\Model;


class Carshop extends Model
{
    //城市关联
    public function City()
    {
        return $this->hasOne('city', 'id', 'city_id')->field(['city_name', 'id']);
    }

    /**
     * @param int $pid  父级id
     * @param $type     水卡类型  1是洗车卡 2是水卡
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public  static function getCard($pid=0,$type)
    {
        $Carshops = \app\common\model\Carshop::where('pid', $pid)->where('id','<>',88)->where('type', $type)->select();

        foreach ($Carshops as $v) {
            $v->city;
        }
        return $Carshops->toArray();
    }


}