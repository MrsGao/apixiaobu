<?php


namespace app\common\model;


use think\facade\Db;
use think\Model;

class Sharingmd extends Model
{
    /** 拉取未审核门店的数据
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function SharingNotLists()
    {
        return Sharingmd::where('status', 0)->where('types', 2)->select();
    }

    /** 获取审核门店内容
     * @param $page
     * @param $limit
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function SharingNotListsPage($page,$limit,$status,$types)
    {
        //统计所有数据
        $cunct =    Sharingmd::order('id desc')->where('status', $status)->where('types',$types)->count();
        //返回数据
        $Sharingmd =  Sharingmd::order('id desc')->where('status', $status)->where('types',$types)->page($page,$limit)->select();

        $arr = [
           'count'=>$cunct,
           'Sharingmds'=>$Sharingmd,
       ] ;
       return $arr;
    }
    //删除未审核门店数据
    public static function DelSharing($id)
    {
        return Sharingmd::where('id', $id)->delete();
    }

    /** 单挑数据返回
     * @param $id
     * @return array|Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function SharingOne($id)
    {
        return Sharingmd::where('id', $id)->find();
    }

    /** 单挑数据更新
     * @param $id
     * @param $data
     */
    public static function updateFindSharing($id, $data)
    {
        return Db::name('Sharingmd')->where('id', $id)->strict(false)->update($data);
    }

    /**
     *  更新门店到洗车卡
     * @param $carshop_id->门店id
     * @param $id->洗车卡id
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function UpdateCards($carshop_id, $id)
    {
        foreach ($carshop_id as $v) {

            $carshops = Db::name('carshop')->where('id', $v)->find();

            if (empty($carshops['sharingmd_id'])) {
                Db::name('carshop')->where('id', $v)->update(['sharingmd_id' => $id]);
            } else {
                $carshop_id = explode(",", $carshops['sharingmd_id']);
                array_push($carshop_id, $id);
                $c = implode(",", $carshop_id);
                Db::name('carshop')->where('id', $v)->update(['sharingmd_id' => $c]);
            }
        }
    }

    /**  更新 swith 状态
     * @param $id
     * @param $num
     * @param $Db
     * @return int
     * @throws \think\db\exception\DbException
     */
    public static function SwitchUpdates($id,$num,$Db){
      return  Db::name('sharingmd')->where('id',$id)->update([$Db=>$num]);
    }
}