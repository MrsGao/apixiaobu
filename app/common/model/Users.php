<?php


namespace app\common\model;


use app\api\business\Token;
use think\Model;

class Users extends Model
{

    public static function getAdminLogin($user,$passwor){
        if (empty($user) || empty($passwor)){
            return show(config('status.error'),'传值不能为空');
        }else{

            $user =   Users::where('username',trim($user))->where('password',trim(md5($passwor)))->find();
            //封装返回参数
            $tokens = [
                'id'=>$user['id']
            ];
           $token =  createToken($tokens);
            $rel = [
                'userName'=>$user['nicheng'],
                'icon'=>$user['icon'],
                'email'=>$user['email'],
                'token'=>$token
            ];
            return $rel;
        }

    }

}