<?php


namespace app\common\lib;


use Tools\Spread;

class ExportExcel
{
    public function excel($expTableData)
    {

        $fileName = "实体店审核";
        $Excel['fileName']=$fileName.date('Y年m月d日-His',time());//or $xlsTitle
        $Excel['cellName']=['A','B','C','D','E','F','G','H','I',
            'J' ,'K' ,'L','M' ,'N','O','P' ,'Q', 'R' ,'S' ,'T' ,'U','V','W' ,'X' ,'Y' ,'Z'
        ];
        $Excel['H'] = ['A'=>12,'B'=>22,'C'=>28,'D'=>38,'E'=>26,'F'=>27,'G'=>29];//横向水平宽度
        $Excel['V'] = ['1'=>40,'2'=>26];//纵向垂直高度
        $Excel['sheetTitle']=$fileName;//大标题，自定义
        $Excel['xlsCell']=[
            ['id','编号'],
            ['name','门店名称'],
            ['fuzeren','负责人'],
            ['iphone','电话号码'],
            ['address','联系地址'],
            ['time','注册时间'],
            ['lng','经纬度']
        ];
        return Spread::excelPut($Excel,$expTableData);
        return show(1,'请求成功',1);

    }
}