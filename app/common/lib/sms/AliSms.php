<?php

declare(strict_types=1);
namespace app\common\lib\sms;
use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use think\facade\Log;


class AliSms implements SmsBase
{
    /**
     * 阿里短信验证码配送
     * ring $iphone
     * @param int $code
     * @return bool
     * @throws ClientException
     *
     */
    public static  function sendCode(string  $iphone,int $code) : bool{
        AlibabaCloud::accessKeyClient(config('Aliconfig.accessKey_id'), config('Aliconfig.access_secret'))
            ->regionId(config('Aliconfig.region_id'))
            ->asDefaultClient();

        $TemplateParam =[
            'code'=>$code
        ];
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host(config('Aliconfig.host'))
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $iphone,
                        'SignName' => config('Aliconfig.sign_name'),
                        'TemplateCode' => config('Aliconfig.template_code'),
                        'TemplateParam' => json_encode($TemplateParam),
                    ],
                ])
                ->request();
            Log::info('alisms-sendCode'.$iphone.'result-'.json_encode($result->toArray()));
            return  true;
            // print_r($result->toArray());
        } catch (ClientException $e) {
            Log::error('alisms-sendCode'.$iphone.'result-'.json_encode($e->getErrorMessage()));
            return  false;
            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            Log::error('alisms-sendCode'.$iphone.'result-'.json_encode($e->getErrorMessage()));
            return  false;
            echo $e->getErrorMessage() . PHP_EOL;
        }

        return  true;
    }
}