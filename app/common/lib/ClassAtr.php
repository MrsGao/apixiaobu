<?php

//  基础类库
namespace app\common\lib;


class ClassAtr
{
    public static function smsClassStat(){
        return [
          'Ali'=>"app\common\lib\sms\AliSms",
          'jd'=>"app\common\lib\sms\JdSms",
        ];
    }

    /**
     * @param $type
     * @param $class
     * @param array $parms
     * @param false $needInstance
     * @return false|mixed
     */
    public static function initClass($type,$class,$parms=[],$needInstance= false){
        /**
         * 如果我们的工厂模式调用的方法是静态的。我们直接返回 类库名字
         * 如果我们调用的不是静态的，我们就要返回对象
         */
        //halt($class);
        //检查数组里是否有指定的键名或索引
        if (!array_key_exists($type,$class)){
            return false;
        }else{
            //new \Reflection($className)=》建立反射机制
            //newInstanceArgs($parms)ß=》相当于实例化对象
            $className= $class[$type];

            return  $needInstance == true?(new \Reflection($className))->newInstanceArgs($parms):$className;
        }
    }
}