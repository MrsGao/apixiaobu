<?php
/*
 *  验证数字
 *
 */

namespace app\common\lib;


class Num
{
  public static function getCode($len){
        if ($len == 4){
         return   rand(1000,9999);
        }else{
         return   rand(100000,999999);
        }
  }
}