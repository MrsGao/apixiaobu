<?php


namespace app\common\controller;


use app\wechat\model\User;
use think\Exception;

class Wechat
{
  public static function wxLogin($code){
      $appid = config('wechat.Appid');
      $secret = config('wechat.secret');
      $sendUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=".$appid."&secret=".$secret."&js_code=".$code."&grant_type=authorization_code";
      $result = curl_get($sendUrl);
      $wxResult = json_decode($result, true);
      if (empty($wxResult)) {
          // 为什么以empty判断是否错误，这是根据微信返回
          // 规则摸索出来的
          // 这种情况通常是由于传入不合法的code
          throw new Exception('获取session_key及openID时异常，微信内部错误');
      }
      return $result ;
  }

    /** 微信的得到电话号码
     * @param $res
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
  public static function getPhoneNumber($res){
      $appid = config('wechat.Appid');
      $sessionKey = $res['session_key'];
      $encryptedData = $res['encryptedData'];
      $iv = $res['iv'];
      $openid = $res['openid'];
      $data = '';
      $pc = new WXBizDataCrypts($appid, $sessionKey);
      $errCode = $pc->decryptData($encryptedData, $iv, $data);

      if ($errCode == 0) {
          //解析微信数据
          $user = json_decode($data);

          $token = User::createUser($user->phoneNumber,$openid);

         return $token;
      } else {
          echo $errCode;

      }
  }
}