<?php


namespace app\common\controller;


use app\BaseController;
use think\captcha\facade\Captcha;

class Verify extends BaseController
{
    public function index(){
        //halt(1);
        return Captcha::create("verify");
    }
}