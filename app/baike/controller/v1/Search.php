<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\middleware\SearchValidate;
use app\BaseController;
use \app\baike\model\Topic as TopicModel;
use \app\baike\model\Post as PostModel;
use \app\baike\model\User as UserModel;

class Search extends BaseController
{
// 搜索话题
    public function topic(){

        (new SearchValidate())->goCheck();
        $list = (new TopicModel)->Search();
        return  show(1,'搜索成功',['list'=>$list]);

    }

    // 搜索文章
    public function post(){

        (new SearchValidate())->goCheck();
        $list = (new PostModel)->Search();
        return  show(1,'搜索成功',$list);

    }

    // 搜索用户
    public function user(){

        (new SearchValidate())->goCheck();
        $list = (new UserModel())->Search();
        return  show(1,'搜索成功',['list'=>$list]);

    }
}