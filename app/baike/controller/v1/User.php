<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\model\Userinfo;
use app\baike\validate\UserValidate;
use \app\baike\model\User as UserModel;
use app\BaseController;
use app\Request;

class User extends BaseController
{
    //发送验证码
    public function sendCode(Request  $request){
        ( new UserValidate())->goCheck('phone');
        //获取验证码
       $phobeNumber = $request->param('phone','','trim');

        UserModel::sendCodes($phobeNumber,4,'Ali');
        return show(1,"发送成功") ;
    }
    // 手机号码登录
    public function phoneLogin(){
        // 验证登录信息
        (new UserValidate())->goCheck('phoneLogin');
        // 手机登录
        $token = (new UserModel())->phobindPhoneneLogin();
        return  show(1,'登录成功',$token);
    }

    // 账号密码登录
    public function Login(){
        // 验证登录信息
        (new UserValidate())->goCheck('login');
        // 登录
        $token = (new UserModel())->login();
        return show (1,'登录成功',$token);
    }

    // 退出登录
    public function Logout(){
        // 退出登录
        (new UserModel())->logout();
    }

    // 用户发布文章列表
    public function post(){

        (new UserValidate())->goCheck('post');
        $list = (new UserModel())->getPostList()->toArray();
        return show (1,'获取成功',['list'=>$list]);

    }

    // 用户发布文章列表
    public function Allpost(){

        (new UserValidate())->goCheck('allpost');
        $list = (new UserModel())->getAllPostList();
        return show (1,'获取成功',['list'=>$list]);

    }
    // 绑定手机
    public function bindPhone(){
        (new UserValidate())->goCheck('bindPhone');
        // 绑定
        $token =   (new UserModel())->bindphone();
        return show (1,'绑定成功',['token'=>$token]);
    }

    // 绑定邮箱
    public function bindEmail(){

        (new UserValidate())->goCheck('bindEmail');
        // 绑定
        $token =    (new UserModel())->bindemail();
        return show (1,'邮箱绑定成功',['token'=>$token]);
    }
    // 修改头像
    public function editUserpic(){
        (new UserValidate())->goCheck('edituserpic');
       $user =  (new UserModel())->editUserpic();
        return show (1,'修改头像成功',$user);

    }

    // 修改资料
    public function editinfo(){
        (new UserValidate())->goCheck('edituserinfo');
        $Userinfo = (new UserModel())->editUserinfo();
        return show (1,'修改成功',$Userinfo);

    }

    // 修改密码
    public function rePassword(){
        (new UserValidate())->goCheck('repassword');
        (new UserModel())->repassword();
        return show (1,'修改密码成功');

    }

    // 关注
    public function follow(){
        (new UserValidate())->goCheck('follow');
        (new UserModel())->ToFollow();
        return show (1,'关注成功');

    }
    // 关注
    public function showUser(){

        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $user_id = request()->userId;
        $Userinfo =  Userinfo::where('user_id',$user_id)->find();

        return show (1,'获取信息',$Userinfo);

    }
    // 取消关注
    public function unfollow(){
        (new UserValidate())->goCheck('unfollow');
        (new UserModel())->ToUnFollow();
        return show (1,'取消关注成功');

    }
    // 互关列表
    public function friends(){
        (new UserValidate())->goCheck('getfriends');
        $list = (new UserModel())->getFriendsList();
        return show (1,'获取成功',['list'=>$list]);

    }

    // 粉丝列表
    public function fens(){
        (new UserValidate())->goCheck('getfens');
        $list = (new UserModel())->getFensList();
        return show (1,'获取成功',['list'=>$list]);

    }

    // 关注列表
    public function follows(){
        (new UserValidate())->goCheck('getfollows');
        $list = (new UserModel())->getFollowsList();
        return show (1,'获取成功',['list'=>$list]);
    }

    // 关注列表
    public function follwpost(){
        (new UserValidate())->goCheck('getfollows');
        $list = (new UserModel())->getFollowPostList();
        return show (1,'获取成功',['list'=>$list]);
    }

// 绑定手机
    public function userInfos(){

        (new UserValidate())->goCheck('getunerinfo');
        // 绑定
        $useinfo =   (new UserModel())->getuserinfo();
        return show (1,'获取信息',['list'=>$useinfo]);
    }

}