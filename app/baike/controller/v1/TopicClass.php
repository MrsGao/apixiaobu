<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\middleware\TopicClassValidate;
use app\BaseController;
use \app\baike\model\TopicClass as TopicClassModel;
class TopicClass extends BaseController
{
    public function index()
    {

        // 获取话题分类列表

        $list=TopicClassModel::getTopicClassList();
        return  show(1,'返回成功',['list'=>$list]);

    }

    // 获取指定话题分类下的话题列表
    public function Topic(){
        // 验证分类id和分页数

        (new TopicClassValidate())->goCheck();
        $list=(new TopicClassModel)->getTopic();
        return  show(1,'返回成功',['list'=>$list]);

    }
}