<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;

//话题模块接口
use app\baike\validate\TopicClassValidate;
use app\BaseController;
use \app\baike\model\Topic as TopicModel;
class Topic   extends BaseController


{
// 获取10个话题
    public function index()
    {
        $list =TopicModel::gethotlist();
        return   show(1,'返回成功',['list'=>$list]);

    }

// 获取指定话题下的文章列表
    public function post()
    {
        // 验证分类id和分页数
        (new TopicClassValidate())->goCheck();
        $list=(new TopicModel)->getPost();
        return  show(1,'获取成功',['list'=>$list]);

    }

}