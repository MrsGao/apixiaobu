<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\validate\CommentValidate;
use app\BaseController;
use \app\baike\model\Comment as CommentModel;

class Comment extends BaseController
{
// 用户评论
    public function comment(){

        (new CommentValidate())->goCheck();
       (new CommentModel())->comment();

        return  show(1,'评论成功');

    }
}