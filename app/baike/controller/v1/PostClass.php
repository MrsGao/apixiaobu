<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\validate\TopicClassValidate;
use app\BaseController;
use \app\baike\model\PostClass as  PostClassModel;
class PostClass extends BaseController
{
    public function index()
    {
        // 获取文章分类列表
        $list=PostClassModel::getPostClassList();
        return  show(1,'返回成功',['list'=>$list]);
    }

    // 获取指定分类下的文章
    public function post()
    {

        // 验证分类id和分页数
        (new TopicClassValidate())->goCheck();

        $list=(new PostClassModel)->getPost();
        return  show(1,'返回成功',$list);

    }
}