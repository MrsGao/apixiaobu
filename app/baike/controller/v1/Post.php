<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\validate\PostValidate;
use \app\baike\model\Post as PostModel;
use app\BaseController;

class Post extends BaseController
{
// 发布文章
    public function Create(){

        //(new PostValidate())->goCheck('create');

        (new PostModel) -> createPost();
        return show(1,'发布成功');
    }

    // 文章详情
    public function Index()
    {

        // 验证文章id
        $PostValidate = new PostValidate();
        $PostValidate ->goCheck('detail');
        $detail = (new PostModel) -> getPostDetail();
        return  show(1,'查询成功',['detail'=>$detail]);

    }
    // 文章评论列表
    public function comment(){
        // 验证文章id
        (new PostValidate())->goCheck('detail');
        $list = (new PostModel) -> getComment();
        return  show(1,'获取成功',['detail'=>$list]);

    }
}