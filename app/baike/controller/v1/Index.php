<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\BaseController;

use think\facade\Cache;
use think\facade\Db;

class Index extends BaseController
{
    //接受用户的订单信息并写入队列的一个文件
    public function acceptNews(){
        $redis=    new \Redis();
        $redis->connect('127.0.0.1',6379);
        $redis->auth('gaobin');
        $redis_name = 'miaosha';

        //死循环操作
        while (1){
            //从队列最左侧去除一个值来
            $user = $redis->lPop($redis_name);
            //然后判断这个值是否存在
            if (!$user || $user == 'nil'){
                echo "执行完毕";
                continue;
            }
            //切割出时间  uid
            $user_arr = explode('%',$user);
            //保存到数据中
           $relt =  Db::name('redis_queue')->insertGetId([
                'uid'=>$user_arr[0],
                'time_stamp'=>$user_arr[1]
            ]);
            //数据库插入的时候回滚机制
            if (!$relt){
                $redis->rPush($redis_name,$user);
            }

        }
        //关闭redis
        $redis->close();
    }
    //配送系统处理队列文件
    public function handNews(){
    $redis=    new \Redis();
    $redis->connect('127.0.0.1',6379);
    $redis->auth('gaobin');
       //秒杀名字
        $redis_name = 'miaosha';
        for ($i= 1;$i<1000;$i++){
            $uid = rand(1,1000);
            //判断长度
            if ($redis->lLen($redis_name) <10){
                $redis->rPush($redis_name,$uid.'%'.microtime());
                echo $uid."秒杀成功";
            }else{
                echo "秒杀结束";
            }
        }
        //管理redis
        $redis->close();
    }
}