<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\validate\FeedbackValidate;
use app\BaseController;
use \app\baike\model\Feedback as FeedbackModel;

class Feedback extends BaseController
{
// 反馈信息
    public function feedback(){
        (new FeedbackValidate())->goCheck('feedback');
        (new FeedbackModel())->feedback();
            return show(1,'反馈成功');
    }

    // 获取用户反馈列表
    public function feedbacklist(){
        (new FeedbackValidate())->goCheck('feedbacklist');
        $list = (new FeedbackModel())->feedbacklist();
        return show(1,'获取成功',[ 'list' => $list ]);

    }
}