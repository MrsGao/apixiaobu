<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\validate\UpdateValidate;
use app\BaseController;
use app\baike\model\Update as UpdateModel;

class Update extends BaseController
{
// 检查更新
    public function update(){
        (new UpdateValidate())->goCheck();
        $res = (new UpdateModel())->appUpdate();
        return   show(1,'ok',$res);

    }
}