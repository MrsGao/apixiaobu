<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller\v1;


use app\baike\middleware\BlacklistValidate;
use app\BaseController;
use \app\baike\model\Blacklist as BlacklistModel;
class Blacklist extends BaseController
{
// 加入黑名单
    public function addBlack(){

        (new BlacklistValidate())->goCheck();

        (new BlacklistModel())->addBlack();
        return  show(1,'加入黑名单成功');

    }
    // 移除黑名单
    public function removeBlack(){
        (new BlacklistValidate())->goCheck();
        (new BlacklistModel())->removeBlack();
        return  show(1,'移除黑名单成功');

    }
}