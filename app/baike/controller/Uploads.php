<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\controller;


use app\BaseController;
use OSS\Core\OssException;
use OSS\OssClient;
use think\facade\Config;
use think\Image;
use think\Image as ImageUpload;
use think\Request;

class Uploads extends BaseController
{

// 单图片处理
    public function imgUpload()
    {
        //接受传过来的头部信息
        $file = \think\facade\Request::file('file');
        //进行图片大小验证
        $info = $file->validate(
            [
                'size' => 500000000,  //文件大小 100万字节是约等于1M
                'ext' => 'jpeg,png,jpg,gif'  //文件扩展名
            ])->move('uploads/');
        //移动到public/uploads目录下面
        if ($info) {
            return [
                "code" => 0,
                "msg" => '',
                "data" => [
                    "src" => '/uploads/' . $info->getSaveName()
                ],
            ];

        } else {
            return [
                "code" => 1,
                "message" => $file->getError(),
            ];
        }


    }

    //图片删除
    public function imgUploadDel(Request $request)
    {
        //接受传过来的头部信息
        $file = $request->param();
        $result = unlink($file);

        //移动到public/uploads目录下面
        if ($result) {
            $arr = [
                "code" => 1,
                "msg" => '删除成功',

            ];
            return json_encode($arr, JSON_UNESCAPED_UNICODE);

        } else {


            $arr = [
                "code" => 0,
                "message" => '删除失败',
            ];
            return json_encode($arr, JSON_UNESCAPED_UNICODE);
        }


    }

    // 单图上传


    public function DoupLoad()
    {
        $file = request()->file('file');  //获取到上传的文件

        $resResult = Image::open($file);
        // 尝试执行
        try {

            //实例化对象 将配置传入
            $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
            //这里是有sha1加密 生成文件名 之后连接上后缀
            $fileName = 'xbht/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
            //执行阿里云上传

            $result = $ossClient->uploadFile('xiaobuvip', $fileName, $file->getInfo()['tmp_name']);

            /**
             * 这个只是为了展示
             * 可以删除或者保留下做后面的操作
             */
            $arr = [
                '图片地址:' => $result['info']['url'],
                'fileName' => $fileName
            ];

        } catch (OssException $e) {
            return $e->getMessage();
        }
        //将结果输出
        return [
            "code" => 0,
            "msg" => '',
            "data" => [
                "src" => 'https://czs.budaohuaxia.com/' . $arr['fileName']
            ],
        ];

    }

    //车型上传
    public function VehicleUploading()
    {
        //halt(1);
        $user_logo = input("file");

        $file = request()->file('file');  //获取到上传的文件

        $resResult = Image::open($file);

        // 尝试执行
        try {
            $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
            //这里是有sha1加密 生成文件名 之后连接上后缀
            $fileName = 'xbcx/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
            //执行阿里云上传

            $result = $ossClient->uploadFile('xiaobuvip', $fileName, $file->getInfo()['tmp_name']);
            //halt($result['info']['url']);
            /**
             * 这个只是为了展示
             * 可以删除或者保留下做后面的操作
             */
            $arr = [
                '图片地址:' => $result['info']['url'],
                'fileName' => $fileName
            ];

        } catch (OssException $e) {
            return $e->getMessage();
        }
        //将结果输出/*/
        return [
            "code" => 0,
            "msg" => '',
            "data" => [
                "src" => 'https://czs.budaohuaxia.com/' . $arr['fileName']
            ],
        ];
    }
    /* OSS图片上传    * liwei    * */
    //实例化对象 将配置传入                    $ossClient = new OssClient($config['KeyId'], $config['KeySecret'], $config['Endpoint']);                    $result = $ossClient->uploadFile($config['Bucket'], $ossFileName, $filePath);                    $arr = [                        'oss_url' => $result['info']['url'],  //上传资源地址                        'relative_path' => $ossFileName     //数据库保存名称(相对路径)                    ];                } catch (OssException $e) {                    return $e->getMessage();                }finally {                    unlink($filePath);                }                $this->jsonReturn(0, '成功oss', array('file' => $arr['oss_url']));            }            $this->jsonReturn(400, $result['msg']);        }

    public function oss_uploadImage($data)
    {

        $result = $this->new_base64_upload($data);

        if ($result['code'] === 200) {
            $fileResult = &$result['data'];
            $filePath = $fileResult['path'] . $fileResult['name'];
            $ossFileName = implode('/', ['upload/image', date('Ymd'), $fileResult['name']]);
            try {
                $config = config('aliyun_oss');
                //实例化对象 将配置传入
                $ossClient = new OssClient($config['KeyId'], $config['KeySecret'], $config['Endpoint']);
                $result = $ossClient->uploadFile($config['Bucket'], $ossFileName, $filePath);
                $arr = ['oss_url' => $result['info']['url'],  //上传资源地址
                    'relative_path' => $ossFileName
                    //数据库保存名称(相对路径)
                ];
            } catch (OssException $e) {
                return $e->getMessage();
            } finally {
                unlink($filePath);
            }
            return array('file' => $arr['oss_url']);
        }
        return $result['msg'];


    }


    /*
     * 将Base64数据转换成二进制并存储到指定路径
     * @param  $base64
     * * @param string $path * liwei     * @return array
     */
    public function new_base64_upload($base64, $path = '')
    {

        $data = explode(',', $base64);

        trace($data, 'api');


        if (preg_match('/^(data:\s*image\/(\w+);base64)/', $base64, $result)) {
            $type = $result[2];
            halt($data[0]);
            if (!in_array($type, array('jpeg', 'jpg', 'gif', 'bmp', 'png'))) {
                return ['code' => 400, 'msg' => '文件格式不在允许范围内'];
            }
            $image_name = md5(uniqid()) . '.' . $result[2];
            $image_path = "./upload/posts/";
            $image_file = $image_path . $image_name;
            //服务器文件存储路径
            try {
                if (file_put_contents($image_file, base64_decode($data[1]))) {
                    return ['code' => 200, 'msg' => '成功', 'data' => ['name' => $image_name, 'path' => $image_path]];
                } else {
                    return ['code' => 400, 'msg' => '文件保存失败'];
                }
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                return ['code' => 400, 'msg' => $msg];
            }
        }

    }


    public function DoupLoads()
    {

        $file = request()->file('file');  //获取到上传的文件
        $resResult = Image::open($file);
        // 尝试执行
        try {
            $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
            //这里是有sha1加密 生成文件名 之后连接上后缀
            $fileName = 'xbht/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
            //执行阿里云上传

            $result = $ossClient->uploadFile('xiaobuvip', $fileName, $file->getInfo()['tmp_name']);

            /**
             * 这个只是为了展示
             * 可以删除或者保留下做后面的操作
             */
            $arr = [
                '图片地址:' => $result['info']['url'],
                'fileName' => $fileName
            ];

        } catch (OssException $e) {
            return $e->getMessage();
        }
        //将结果输出
        return [
            "code" => 0,
            "msg" => '',
            "data" => [
                "src" => 'https://czs.budaohuaxia.com/' . $arr['fileName']
            ],
        ];

    }

    public function Imgdemo(Request $request)
    {
        //接收上传的文件
        $file = $this->request->file('file');

        if (!empty($file)) {
            //图片存的路径
            $imgUrl = ROOT_PATH . 'public' . DS . 'uploads' . '/' . date("Y/m/d");

            // 移动到框架应用根目录/public/uploads/ 目录下

            $info = $file->validate(['size' => 1048576, 'ext' => 'jpg,png,gif'])->rule('uniqid')->move($imgUrl);
            $error = $file->getError();
            //验证文件后缀后大小
            if (!empty($error)) {
                dump($error);
                exit;
            }
            if ($info) {
                // 成功上传后 获取上传信息
                //获取图片的名字
                $imgName = $info->getFilename();
                //获取图片的路径
                $photo = $imgUrl . "/" . $imgName;

            } else {
                // 上传失败获取错误信息
                $file->getError();
            }
        } else {
            $photo = '';
        }
        if ($photo !== '') {
            return ['code' => 1, 'msg' => '成功', 'photo' => $photo];
        } else {
            return ['code' => 404, 'msg' => '失败'];
        }

    }


    // 单图片处理
    public function imgUploads()
    {
        //接受传过来的头部信息
        $file = \think\facade\Request::file('file');
        //进行图片大小验证
        $info = $file->validate(
            [
                'size' => 500000000,  //文件大小 100万字节是约等于1M
                'ext' => 'jpeg,png,jpg,gif'  //文件扩展名
            ])->move('uploads/');
        //移动到public/uploads目录下面
        if ($info) {
            return [
                "code" => 0,
                "msg" => '',
                "data" => [
                    "src" => '/uploads/' . $info->getSaveName()
                ],
            ];

        } else {
            return [
                "code" => 1,
                "message" => $file->getError(),
            ];
        }


    }

    public function DoupLoadss()
    {
        $file = request()->file('file');  //获取到上传的文件

        $resResult = Im::open($file);
        // 尝试执行
        try {
            $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
            //这里是有sha1加密 生成文件名 之后连接上后缀
            $fileName = 'xbht/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
            //执行阿里云上传

            $result = $ossClient->uploadFile('xiaobuvip', $fileName, $file->getInfo()['tmp_name']);

            /**
             * 这个只是为了展示
             * 可以删除或者保留下做后面的操作
             */
            $arr = [
                '图片地址:' => $result['info']['url'],
                'fileName' => $fileName
            ];

        } catch (OssException $e) {
            return $e->getMessage();
        }
        //将结果输出
        return [
            "code" => 0,
            "msg" => '',
            "data" => [
                "src" => '/' . $arr['fileName']
            ],
        ];

    }


    //小程序图片上传
    public function WxUploading()
    {
        //halt(1);
        $user_logo = input("file");

        $file = request()->file('file');  //获取到上传的文件

        $resResult = Image::open($file);
        //halt($resResult);
        // 尝试执行
        try {
            $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
            //这里是有sha1加密 生成文件名 之后连接上后缀
            $fileName = 'xbcx/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
            //执行阿里云上传

            $result = $ossClient->uploadFile('xiaobuvip', $fileName, $file->getInfo()['tmp_name']);
            /**
             * 这个只是为了展示
             * 可以删除或者保留下做后面的操作
             */
            $arr = [
                '图片地址:' => $result['info']['url'],
                'fileName' => $fileName
            ];

        } catch (OssException $e) {
            return $e->getMessage();
        }


        return 'https://czs.budaohuaxia.com/' . $arr['fileName'];


    }

    public function Imgdemos(Request $request)
    {
        //接收上传的文件
        $file = $this->request->file('file');

        if (!empty($file)) {
            //图片存的路径

            // 移动到框架应用根目录/public/uploads/ 目录下

            $info = $file->move('./uploads');
            //路径名称 20210127/2b044c6ce12799edf2d249ed5f761e0f.pdf
            $imgName = $info->getSaveName();

            // 文件后缀;
            $resResult = $info->getExtension();

            try {
                $ossClient = new OssClient(Config::get('KeyId'), Config::get('KeySecret'), Config::get('Endpoint'));
                //这里是有sha1加密 生成文件名 之后连接上后缀
                $fileName = 'bdxy/' . sha1(date('YmdHis')) . '.' . $resResult;
                //执行阿里云上传

                $result = $ossClient->uploadFile('xiaobuvip', $fileName, './uploads/'.$imgName);

                /**
                 * 这个只是为了展示
                 * 可以删除或者保留下做后面的操作
                 */
                $arr = [
                    '图片地址:' => $result['info']['url'],
                    'fileName' => $fileName
                ];

            } catch (OssException $e) {
                return $e->getMessage();
            }
            //将结果输出
            return [
                "code" => 1,
                "msg" => '',
                "data" => [
                    "src" => 'https://czs.budaohuaxia.com/' . $arr['fileName']
                ],
            ];


        }


    }


    // 上传图片
    public static function AliUpload($files,$savename){
        // 获取图片



        if ($files) {
            // 多图上传
            $arr = [];

            $resResult = ImageUpload::open($files);

            // 尝试执行
            try {

                //实例化对象 将配置传入
                $ossClient = new OssClient(config('alioss.KeyId'), config('alioss.KeySecret'), config('alioss.Endpoint'));
                //这里是有sha1加密 生成文件名 之后连接上后缀
                $fileName = 'xbht/' . sha1(date('YmdHis', time()) . uniqid()) . '.' . $resResult->type();
                //执行阿里云上传

                $result = $ossClient->uploadFile('xiaobuvip', $fileName, $savename);

                /**
                 * 这个只是为了展示
                 * 可以删除或者保留下做后面的操作
                 */
                unlink($savename);
                $arr = [
                    '图片地址:' => $result['info']['url'],
                    'fileName' => config('Aliconfig.doman'). $fileName
                ];

            } catch (OssException $e) {
                return $e->getMessage();
            }
            //将结果输出
            return  $arr;

        }



    }

}