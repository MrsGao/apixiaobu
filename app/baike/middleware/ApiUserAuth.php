<?php
declare (strict_types = 1);

namespace app\baike\middleware;

use app\baike\lib\exception\BaseException;
use think\facade\Cache;

class ApiUserAuth
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 获取头部信息
        $param = $request->header();

        // 不含token
        if (!array_key_exists('token',$param)) throw new BaseException(['code'=>200,'message'=>'非法token，禁止操作','errorCode'=>20003]);
        // 当前用户token是否存在（是否登录）
        $token = $param['token'];

        $user = authToken($token);

        if ($user['code'] == 1 ) {

            // 将token和userid这类常用参数放在request中
            $request->userToken = $token;
            $request->userId =  $user['user']['id'];
            $request->userTokenUserInfo = $user;
            return $next($request);
        }

    }
}
