<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\middleware;


use app\baike\validate\Basevalidate;

class SearchValidate extends Basevalidate
{
    protected $rule = [
        'keyword'=>'require|chsDash',
        'page'=>'require|integer|>:0',
    ];
}