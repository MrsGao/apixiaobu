<?php
declare (strict_types = 1);

namespace app\baike\middleware;

use app\baike\model\User;

class ApiUserBindPhone
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //
        $param = $request->userTokenUserInfo;
        $user =  new User();
        $user->OtherLoginIsBindPhone($param);

        return $next($request);
    }
}
