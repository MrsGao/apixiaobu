<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\lib\exception;


use Throwable;

class BaseException extends \Exception
{
    public $code = 400;
    public $msg = "异常";
    public $errorCode = 999;
    public function __construct($params=[])
    {
        if (!is_array($params)) return;
        if (array_key_exists('code',$params)) $this->code = $params['code'];
        if (array_key_exists('message',$params)) $this->msg = $params['message'];
        if (array_key_exists('errorCode',$params)) $this->errorCode = $params['errorCode'];



    }
}