<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\lib\exception;


use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\Response;
use Throwable;


class ExceptionHandler extends  Handle
{
    public $code ;
    public $msg ;
    public $errorCode;
    //继承方法然后重写
    public function render($request, Throwable $e): Response
    {
        if ($e instanceof BaseException){
            $this->code = $e->code;
            $this->msg = $e->msg;
            $this->errorCode = $e->errorCode;
        }else{
            //判断是否开启调试模式
            if (env('APP_DEBUG')){
                $this->isJson = $request->isJson();
                if ($e instanceof HttpResponseException) {
                    return $e->getResponse();
                } elseif ($e instanceof HttpException) {
                    return $this->renderHttpException($e);
                } else {
                    return $this->convertExceptionToResponse($e);
                } }

            $this->code = 500;
            $this->msg = "服务器异常";
            $this->errorCode = '999';
        }
        $res = [
            'msg'=>$this->msg,
            'errorCode'=>$this->errorCode,
        ];
        return json($res,$this->code);


    }
    /**第二步在app目录下面的provider.php文件中绑定异常处理类
    'exception_handler' => 'app\baike\lib\exception\ExceptionHandler'
    */


}