<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;




use app\baike\lib\exception\BaseException;
use app\baike\model\Comment;
use app\baike\model\Post;
use app\baike\model\PostClass;
use app\baike\model\Topic;
use app\baike\model\User;

class Basevalidate extends \think\Validate
{
    /**
     * 检测所有客户端发来的参数是否符合验证类规则
     * 基类定义了很多自定义验证方法
     * 这些自定义验证方法其实，也可以直接调用
     * @throws ParameterException
     * @return true
     */
    public function goCheck($scene = false)
    {
        //获取请求传递过来的所有参数
        $params = $this->request->param();
        // 验证场景
        $check = $scene?$this->scene($scene)->check($params):$this->check($params);
        //开始验证
        if (!$check) {
            $error = $this->getError($params);
                //  验证失败 抛出异常
            throw new BaseException(['message'=>$error,'errorCode'=>400]);
        }
        return true;
    }

    /**
     * @param array $arrays 通常传入request.post变量数组
     * @return array 按照规则key过滤后的变量数组
     * @throws ParameterException
     */
    public function getDataByRule($arrays)
    {
        if (array_key_exists('user_id', $arrays) | array_key_exists('uid', $arrays)) {
            // 不允许包含user_id或者uid，防止恶意覆盖user_id外键
            throw new ParameterException([
                'msg' => '参数中包含有非法的参数名user_id或者uid'
            ]);
        }
        $newArray = [];
        foreach ($this->rule as $key => $value) {

            $newArray[$key] = $arrays[$key];
        }

        return $newArray;
    }

    protected function isPositiveInteger($value, $rule='', $data='', $field='')
    {
        if (is_numeric($value) && is_int($value + 0) && ($value + 0) > 0) {
            return true;
        }
        return $field . '必须是正整数';
    }

    protected function isNotEmpty($value, $rule='', $data='', $field='')
    {
        if (empty($value)) {
            return $field . '不允许为空';
        } else {
            return true;
        }
    }

    //没有使用TP的正则验证，集中在一处方便以后修改
    //不推荐使用正则，因为复用性太差
    //手机号的验证规则
    protected function isMobile($value)
    {
        $rule = '^1(3|4|5|7|8)[0-9]\d{8}$^';
        $result = preg_match($rule, $value);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }
    protected function isPefectCode($value, $rule='', $data='', $field='')
    {
        // 验证码不存在
        $beforeCode = cache($data['phone']);
        if(!$beforeCode) return "请重新获取验证码";
        // 验证验证码
        if($value != $beforeCode) return "验证码错误";
        return true;
    }

    // 话题是否存在
    protected function isTopicExist($value, $rule='', $data='', $field='')
    {
        if ($value==0) return true;
        if (Topic::field('id')->find($value)) {
            return true;
        }
        return "该话题已不存在";
    }

// 文章分类是否存在
    protected function isPostClassExist($value, $rule='', $data='', $field='')
    {
        if (PostClass::field('id')->find($value)) {
            return true;
        }
        return "该文章分类已不存在";
    }

    // 评论是否存在
    protected function isCommentExist($value,$rule='',$data='',$field='')
    {
        if ($value==0) return true;
        if (Comment::field('id')->find($value)) {
            return true;
        }
        return "回复的评论已不存在";
    }
    // 用户是否存在
    protected function isUserExist($value, $rule='', $data='', $field='')
    {
        if (User::field('id')->find($value)) {
            return true;
        }
        return "该用户已不存在";
    }

    // 不能为空
    protected function NotEmpty($value, $rule='', $data='', $field='')
    {
        if (empty($value)) return $field."不能为空";
        return true;
    }
    // 文章是否存在
    protected function isPostExist($value, $rule='', $data='', $field=''){
        if (Post::field('id')->find($value)) {
            return true;
        }
        return "该文章已不存在";
    }
}