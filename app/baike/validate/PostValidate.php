<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;


class PostValidate extends Basevalidate
{
    protected $rule = [
        'text'=>'require',
        'imglist'=>'require',
        'isopen'=>'require|in:0,1',
        'topic_id'=>'require|integer|>:0|isTopicExist',
        'post_class_id'=>'require|integer|>:0|isPostClassExist',
        'id'=>'require|integer|>:0',
    ];

    protected $scene = [
        //发布文章验证
        'create'=>['text','imglist','token','isopen','post_class_id'],
        //查询文章验证
        'detail'=>['id']
    ];
    public function index(){
        show(1);
    }
}