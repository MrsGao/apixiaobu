<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;


class ChatValidate extends Basevalidate
{
    protected $rule = [
        'to_id'=>'require|isUserExist',
        'from_userpic'=>'require',
        'type'=>'require',
        'data'=>'require',
        'client_id'=>'require'
    ];

    protected $scene = [
        'send'=>['to_id','from_userpic','type','data'],
        'bind'=>['type','client_id']

    ];
}