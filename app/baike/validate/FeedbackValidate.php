<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;


class FeedbackValidate extends Basevalidate
{
    protected $rule = [
        'data'=>'require|NotEmpty',
        'page'=>'require|integer|>:0'
    ];
    protected $scene = [
        'feedback'=>['data'],
        'feedbacklist'=>['page']
    ];
}