<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;



class CommentValidate extends Basevalidate
{
    protected $rule = [
        'fid'=>'require|integer|>:-1|isCommentExist',
        'data'=>'require',
        'post_id'=>'require|integer|>:0',
    ];
}