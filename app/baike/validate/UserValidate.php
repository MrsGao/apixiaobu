<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\validate;


use think\facade\Cache;

class UserValidate extends Basevalidate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'phone'  => 'require|mobile',
        'age'   => 'number|between:1,120',
        'email' => 'email',
        'code'=>'require|number|length:4|isPefectCode',
        'username'=>'require',
        'password'=>'require',
        'id'=>'require|integer|>:0',
        'userId'=>'require',
        'page'=>'require|integer|>:0',
        'userpic'=>'image',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'phone.require' => '手机号必须存在',
        'phone.mobile'     => '手机号不正确',
        'code.require' => '验证码必须存在',
        'code.number' => '验证码必须是数字',
        'code.length' => '验证码长度必须是四位',
        'page'=>'require|integer|>:0',
        'email'=>'require|email',
        'name'=>'require|chsDash',
        'sex'=>'require|in:0,1,2',
        'qg'=>'require|in:0,1,2',
        'job'=>'require|chsAlpha',
        'birthday'=>'require|dateFormat:Y-m-d',
        'path'=>'require|chsDash',
        'oldpassword'=>'require',
        'newpassword'=>'require|alphaDash',
        'renewpassword'=>'require|confirm:newpassword',
        'follow_id'=>'require|integer|>:0|isUserExist'



    ];
    protected function isPefectCode($value, $rule='', $data='', $field='')
    {

        // 验证码不存在
        $beforeCode = $data['code'];

        if(!$beforeCode) return "请重新获取验证码";
        // 验证验证码
        if($value != $beforeCode) return "验证码错误";
        return true;
    }
    protected $scene = [
        'phone'  =>  ['phone'],
        'phoneLogin'=>['phone','code'],
        'login'=>['username','password'],
        'post'=>['id','page'],
        'allpost'=>['page'],
        'bindPhone'=>['phone'],
        'bindEmail'=>['email'],
        'edituserpic'=>['userpic'],
        'edituserinfo'=>['name','sex','qg','job','birthday','path'],
        'repassword'=>['oldpassword','newpassword','renewpassword'],
        'follow'=>['follow_id'],
        'unfollow'=>['follow_id'],
        'getfriends'=>['page'],
        'getfens'=>['page'],
        'getfollows'=>['page'],
        'getunerinfo'=>['userId']
    ];
}