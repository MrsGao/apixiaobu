<?php

return [
    //token验证通过
    'one'=>1,
    //Token验证不通过,用户不存在
    'zero'=>0,
    //Token过期
    'three'=>3,
    //Token无效
    'four'=>4,
];