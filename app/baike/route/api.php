<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
use \app\baike\middleware\ApiUserAuth ;
use \app\baike\middleware\ApiUserBindPhone;
use \app\baike\middleware\ApiUserStatus;

//验证token
Route::group('/:version/',function(){
// 退出登录
    // 用户评论
    Route::post('post/comment','/:version.Comment/Comment');
    Route::post('User/Logout',':version.User/Logout');

    // 用户顶踩
    Route::post('support', ':version.Support/index');
    // 编辑头像
    Route::post('edituserpic',':version.User/editUserpic');
    // 编辑资料
    Route::post('edituserinfo',':version.User/editinfo');
    // 编辑资料
    Route::post('showUser',':version.User/showUser');
    // 修改密码
    Route::post('repassword',':version.User/rePassword');
    // 加入黑名单
    Route::post('addblack',':version.Blacklist/addBlack');
    // 移出黑名单
    Route::post('removeblack',':version.Blacklist/removeBlack');
    // 关注
    Route::post('follow',':version.User/follow');
    // 取消关注
    Route::post('unfollow',':version.User/unfollow');
    // 互关列表
    Route::get('friends/:page',':version.User/friends');
    // 粉丝列表
    Route::get('fens/:page',':version.User/fens');
    // 关注列表
    Route::get('follows/:page',':version.User/follows');
    // 关注列表
    Route::get('follwpost/:page',':version.User/follwpost');
    // 用户反馈
    Route::post('feedback',':version.Feedback/feedback');
    // 获取用户反馈列表
    Route::get('feedbacklist/:page',':version.Feedback/feedbacklist');
    // 发送信息
    Route::post('chat/send',':version.Chat/send');
    // 接收未接受信息
    Route::post('chat/get',':version.Chat/get');
    // 绑定上线
    Route::post('chat/bind',':version.Chat/bind');
})->middleware([ApiUserAuth::class,ApiUserBindPhone::class,ApiUserStatus::class]);
//只验证token
Route::group('/:version/',function(){
    // 绑定手机
    Route::post('user/bindPhone', '/:version.User/bindPhone');
    // 绑定邮箱
    Route::post('user/bindEmail', '/:version.User/bindEmail');

   })->middleware(ApiUserAuth::class);
// 不需要验证token
Route::group('/:version/',function(){
// 账号密码登录
    Route::post('User/Login','/:version.User/Login');
//发送短信
    Route::post('User/sendCode', '/:version.User/sendCode');
    //手机登录
    Route::post('User/phoneLogin', '/:version.User/phoneLogin');
    Route::post('userInfo',':version.User/userInfos');
    // 获取文章分类
    Route::get('postClass', '/:version.PostClass/index');
    // 获取话题分类
    Route::post('topicClass','/:version.TopicClass/index');
    // 获取热门话题
    Route::get('hotTopic','/:version.Topic/index');
    // 获取指定话题分类下的话题列表
    Route::get('topicClass/:id/Topic/:page', '/:version.TopicClass/Topic');

    Route::post('post/:id','/:version.Post/Index');


// 获取指定话题下的文章列表
    Route::get('topic/:id/post/:page', '/:version.Topic/post');
// 获取指定文章分类下的文章
    Route::post('postClass/:id/post/:page', '/:version.PostClass/post');

    Route::get('user/:id/post/:page', '/:version.User/post');
    // 广告列表
    Route::get('adsense/:type', '/:version.Adsense/index');

// 搜索话题
    Route::post('search/topic', ':version.Search/topic');

    // 搜索文章
    Route::post('search/post', ':version.Search/post');

    // 搜索用户
    Route::post('search/user', ':version.Search/user');
// 获取当前文章的所有评论
    Route::get('post/:id/comment',':version.Post/comment');
    // 检测更新
    Route::post('update',':version.Update/update');
// 搜索文章
    Route::post('acceptnews', ':version.Index/acceptNews');
    Route::post('handNews', ':version.Index/handNews');

});

//图片上传
Route::group('/:version/',function(){
    // 获取热门话题
    Route::post('uploadMore','/:version.Image/uploadMore');

    // 发布文章
    Route::post('Create','/:version.Post/Create');
    // 获取指定用户下的所有文章（含隐私）
    Route::get('user/post/:page', '/:version.User/Allpost');

})->middleware([ApiUserAuth::class,ApiUserStatus::class]);;






