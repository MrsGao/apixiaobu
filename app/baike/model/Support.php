<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use think\facade\Db;
use think\Model;

class Support extends Model
{

// 用户顶踩文章
    public function UserSupportPost(){
        $param = request()->param();
        // 获得用户id
        $userid = request()->userId;
        // 判断是否已经顶踩过
        $support = self::where(['user_id'=>$userid,'post_id'=>$param['post_id']])->find();

        // 已经顶踩过，判断当前操作是否相同

     Db::table('post')->where('id',$param['post_id'])->inc("ding_count");

       // 直接创建
        return $this->create([
            'user_id'=>$userid,
            'post_id'=>$param['post_id'],
            'type'=>$param['type']
        ]);
    }
    // 关联顶踩模型
    public function support(){
        return $this->hasMany(Support::class,'post_id','id');
    }
}