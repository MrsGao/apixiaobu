<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use AlibabaCloud\Client\Request\Request;
use think\Model;

class Post extends Model
{
    //
    public function gettitlepicAttr($value,$data){
        $finalUrl = json_decode($value);
        return $finalUrl[0]->id;
         }
// 关联图片表
    public function images(){
        return $this->belongsToMany('Image','post_image');
    }
// 关联顶踩模型
    public function support(){
        return $this->hasMany(Support::class,'post_id');
    }
// 发布文章
    public function createPost(){
        // 获取所有参数

        $params = request()->param();

        $userModel = new User();
        // 获取用户id
        $user_id = request()->userId;

        $Userinfo = Userinfo::where('user_id',$user_id)->find();

        $path = $Userinfo['path'] ;


        // 发布文章
        $title = mb_substr($params['text'],0,30);
         $this->create([
            'user_id'=>$user_id,
            'title'=>$title,
            'titlepic'=>$params['imglist'],
            'content'=>$params['text'],
            'path'=>$path ? $path : '未知',
            'type'=>0,
            'post_class_id'=>$params['post_class_id'],
            'share_id'=>0,
            'isopen'=>$params['isopen'],
            'post_image'=>$params['imglist']
        ]);

        // 返回成功
        return true;
    }

    // 关联用户表
    public function user(){
        return $this->belongsTo('User');
    }

// 关联分享
    public function share(){
        return $this->belongsTo('Post','share_id','id');
    }

// 获取文章详情
    public function getPostDetail(){
        // 获取所有参数
        $param = request()->param();
        return $this->with(['user'=>function($query){
            return $query->field('id,username,userpic');
        },'images'=>function($query){
            return $query->field('url');
        },'share'])->find($param['id']);
    }

    // 根据标题搜索文章
    public function Search(){
        // 获取所有参数
        $param = request()->param();
        return $this->where('title','like','%'.$param['keyword'].'%')->with(['user'=>function($query){
            return $query->field('id,username,userpic');
        },'images'=>function($query){
            return $query->field('url');
        },'share'])->page($param['page'],10)->select();
    }

    // 关联评论
    public function comment(){
        return $this->hasMany('Comment');
    }
// 获取评论
    public function getComment(){
        $params = request()->param();
        return Comment::where('post_id',$params['id'])->with(['user'=>function($query){return $query->field('id,username,userpic');}])->select();
    }
}