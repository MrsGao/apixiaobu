<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use AlibabaCloud\Client\Request\Request;
use AlibabaCloud\Hiknoengine\V20190625\HiknoengineApiResolver;
use app\baike\controller\FileController;
use OSS\Core\OssException;
use OSS\OssClient;
use think\facade\Config;
use think\Model;
use \think\Image as ImageUpload;

class Image extends Model
{
// 上传多图
    public function uploadMore(){

        $image = $this->upload(request()->userId,'imglist')->toArray();



        return $image;
    }

// 上传图片
    public function upload($userid = '',$field = ''){
        // 获取图片
        $files = request()->file($field);

        if (is_array($files)) {
            // 多图上传
            $arr = [];
            foreach($files as $file){
                $res = FileController::UploadEvent($file);
                    $arr[] = [
                        'url'=>$res['data'],
                        'user_id'=>$userid
                    ];

            }

            return  $this->saveAll($arr);
        }
        // 单图上传
        if(!$files) TApiException('请选择要上传的图片',10000,200);
        // 单文件上传
        $file = FileController::UploadEvent($files);
        // 上传失败
        if(!$file['status']) TApiException($file['data'],10000,200);
        // 上传成功，写入数据库
        return self::create([
            'url'=>$file['data'],
            'user_id'=>$userid
        ]);
    }

    // 图片是否存在
    public function isImageExist($id,$userid){
        return $this->where('user_id',$userid)->field('id')->find($id);
    }
}