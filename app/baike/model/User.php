<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use AlibabaCloud\Client\Request\Request;
use app\baike\lib\exception\BaseException;
use app\common\lib\ClassAtr;
use \app\baike\lib\Num;
use think\app\Url;
use \think\facade\Cache;
use think\facade\Db;
use think\Model;

class User extends Model
{

    // 关联文章
    public function post(){
        return $this->hasMany('Post');
    }

    public function Image(){
        return $this->hasOne(Image::class,'user_id','id');
    }

// 获取指定用户下文章
    public function getPostList(){
        $params = request()->param();
        $user = User::where('id',$params['id'])->find();

        if (!$user) TApiException('该用户不存在',10000);
        return $user->with(['post',
            'Image'])->page($params['page'],10)->select();
    }

    // 发送验证码

    //获取手机号
    public static function sendCodes($iphone,$len,$type) :bool{
        $code = Num::getCode($len);

        Cache::set($iphone,$code);
        /**
         * 第一种方式使用接口
         * 接口集成,实现工厂模式
        $class =  "app\common\lib\sms\\".$type.'Sms';
        $sms =  $class::sendCode($iphone,$code);*/
        /**
         * 第二种使用高大上的反射机制
         */
        $classStats = ClassAtr::smsClassStat();

        $classSobj = ClassAtr::initClass($type,$classStats,[],false);

        $sms = $classSobj::sendCode($iphone,$code);


        if ($sms){
            //短信存储redis时间为一分钟
            \cache(config('redis.code_pre').$iphone,$code,config('redis.code_expire'));

            return true;
        }
    }



    // 绑定用户信息表数据
    public function userinfo(){
        return $this->hasOne('Userinfo');
    }


    // 账号登录
    public function login(){
        // 获取所有参数
        $param = request()->param();
        // 验证用户是否存在

        $user = $this->isExist($this->filterUserData($param['username']));
        // 用户不存在

        if(!$user) throw new BaseException(['code'=>200,'message'=>'昵称/邮箱/手机号错误','errorCode'=>20000]);
        // 用户是否被禁用
        $this->checkStatus($user->toArray());

        // 验证密码
        $this->checkPassword($param['password'],$user->password);
        // 登录成功 生成token，进行缓存，返回客户端
        $user->logintype = 'username';

       $token = createToken($user->toArray());
        $user['token']=$token;

       Cache::set('Token',$token);
        return $user;
    }

    // 验证用户名是什么格式，昵称/邮箱/手机号
    public function filterUserData($data){
        $arr=[];

        // 验证是否是手机号码
        if(preg_match('^1(3|4|5|7|8)[0-9]\d{8}$^', $data)){

            $arr['phone'] = $data;

            return $arr;
        }

        // 验证是否是邮箱
        if(preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/', $data)){
            $arr['email']=$data;
            return $arr;
        }

        $arr['username']=$data;
        return $arr;
    }

// 验证密码

    /**
     * @param $password 客户输入旧密码
     * @param $hash  客户数据库的密码
     * @return bool
     * @throws BaseException
     */
    public function checkPassword($password,$hash){

        if (!$hash) throw new BaseException(['code'=>200,'message'=>'密码错误','errorCode'=>20002]);
        // 密码错误

        if(!($hash == md5($password))) throw new BaseException(['code'=>200,'message'=>'旧密码错误','errorCode'=>20002]);
        return true;
    }

    // 退出登录
    public function logout(){

        if (!Cache::pull(request()->userToken)) throw new BaseException(['code'=>200,'message'=>'你已经退出了','errorCode'=>30006]);
        return true;
    }

    public function OtherLoginIsBindPhone($user)
    {
        // 验证是否是第三方登录

        if ( is_null($user['user']['phone'])) {
            TApiException(200,'请先绑定手机',20008);
        }
        return $user['user']['id'];

        // 账号密码登录
    }

    // 用户是否被禁用（在前面课程的基础上扩充）
    public function checkStatus($arr,$isReget = false){
        $status = 1;

        if ($isReget) {
            // 账号密码登录
            $userid = $arr['user']['id'];
            $user = $this->find($userid)->toArray();
            $status = $user['status'];
        }else{
            $status = $arr['status'];
        }
        if($status==0) throw new BaseException(['code'=>200,'message'=>'该用户已被禁用','errorCode'=>20001]);
        return $arr;
    }

    // 获取指定用户下所有文章
    public function getAllPostList(){
        $params = request()->param();
        // 获取用户id
        $user_id=request()->userId;

        return $this->where('id',$user_id)->with([
            'post','Image'])->page($params['page'],10)->select();
    }

    // 判断用户是否存在（补充前面课程）
    public function isExist($arr=[]){

        if(!is_array($arr)) return false;
        if (array_key_exists('phone',$arr)) { // 手机号码
            $user = $this->where('phone',$arr['phone'])->find();
            if ($user) $user->logintype = 'phone';

            return $user;
        }
        // 用户id
        if (array_key_exists('id',$arr)) { // 用户名
            return $this->where('id',$arr['id'])->find();
        }
        if (array_key_exists('email',$arr)) { // 邮箱
            $user = $this->where('email',$arr['email'])->find();
            if ($user) $user->logintype = 'email';
            return $user;
        }
        if (array_key_exists('username',$arr)) { // 用户名
            $user = $this->where('username',$arr['username'])->find();
            if ($user) $user->logintype = 'username';
            return $user;
        }
        // 第三方参数
        if (array_key_exists('provider',$arr)) {
            $where = [
                'type'=>$arr['provider'],
                'openid'=>$arr['openid']
            ];
            $user = $this->userbind()->where($where)->find();
            if ($user) $user->logintype = $arr['provider'];
            return $user;
        }
        return false;
    }

// 手机登录（补充前面课程）
    public function phoneLogin(){
        // 获取所有参数
        $param = request()->param();
        // 验证用户是否存在
        $user = $this->isExist(['phone'=>$param['phone']]);
        // 用户不存在，直接注册
        if(!$user){
            // 用户主表
            $user = self::create([
                'username'=>$param['phone'],
                'phone'=>$param['phone'],
                // 'password'=>password_hash($param['phone'],PASSWORD_DEFAULT)
            ]);
            // 在用户信息表创建对应的记录（用户存放用户其他信息）
            $user->userinfo()->create([ 'user_id'=>$user->id ]);
            $user->logintype = 'phone';
            $token = createToken($user->toArray());
            $user['token']=$token;

            Cache::set('Token',$token);
            return $user;


        }
        // 用户是否被禁用
        $this->checkStatus($user->toArray());
        // 登录成功，返回token
        $token = createToken($user->toArray());
        $user['token']=$token;

        Cache::set('Token',$token);
        return $user;
    }

// 第三方登录（补充前面课程）
    public function otherlogin(){
        // 获取所有参数
        $param = request()->param();
        // 解密过程（待添加）
        // 验证用户是否存在
        $user = $this->isExist(['provider'=>$param['provider'],'openid'=>$param['openid']]);
        // 用户不存在，创建用户
        $arr = [];
        if (!$user) {
            $user = $this->userbind()->create([
                'type'=>$param['provider'],
                'openid'=>$param['openid'],
                'nickname'=>$param['nickName'],
                'avatarurl'=>$param['avatarUrl'],
            ]);
            $arr = $user->toArray();
            $arr['expires_in'] = $param['expires_in'];
            $arr['logintype'] = $param['provider'];
            return $this->CreateSaveToken($arr);
        }
        // 用户是否被禁用
        $arr = $this->checkStatus($user->toArray(),true);
        // 登录成功，返回token
        $arr['expires_in'] = $param['expires_in'];
        return $this->CreateSaveToken($arr);
    }



// 绑定手机
    public function bindphone(){
        // 获取所有参数
        $params = request()->param();
        $currentUserInfo = request()->userTokenUserInfo;
        $currentUserId = request()->userId;
        // 当前登录类型
        $currentLoginType = $currentUserInfo['user']['logintype'];
        //如果是手机用户
        if ($currentLoginType == 'phone'){
            $binduser = $this->isExist(['phone'=>$params['phone']]);
            if (is_null($binduser)){
                // 直接修改
                $result = User::where('id',$currentUserId)->update(['phone'=>$params['phone']]);
                if ($result){
                    $userbind = User::where('id',$currentUserId)->find();
                    $token = createToken($userbind->toArray());
                    Cache::set('Token',$token);
                    return $token;
                }
            }else{
                TApiException(200,'手机号已被绑定',20006);
            }
        }
    }

    // 绑定邮箱
    public function bindemail(){
        // 获取所有参数
        $params = request()->param();
        $currentUserId = request()->userId;
        // 查询该手机是否绑定了其他用户
        $binduser = $this->isExist(['email'=>$params['email']]);

        // 存在
        if (is_null($binduser)) {

            $result = User::where('id',$currentUserId)->update(['email'=>$params['email']]);
            if ($result){
                $userbind = User::where('id',$currentUserId)->find();
                $token = createToken($userbind->toArray());
                Cache::set('Token',$token);
                return $token;
            }
        }else{
            TApiException(200,'已绑定邮箱',19000);
        }

    }

    // 搜索用户
    public function Search(){
        // 获取所有参数
        $param = request()->param();
        return $this->where('username','like','%'.$param['keyword'].'%')->page($param['page'],10)->with('userinfo')->hidden(['password'])->select();
    }

    //  修改头像
    public function editUserpic(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid=request()->userId;
        $image = (new Image())->upload($userid,'userpic')->toArray();

        // 修改用户头像
        $user = $this->where('id',$userid)->find();

        $user->userpic = $image['url'];
        if($user->save()) return $user;
        TApiException();
    }

    // 修改资料
    public function editUserinfo(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid=request()->userId;
        // 修改昵称
        $user = $this->where('id',$userid)->find();
        $user->username = $params['name'];
        $user->save();
        // 修改用户信息表
        $userinfo = $user->userinfo()->find();
        $userinfo->sex = $params['sex'];
        $userinfo->qg = $params['qg'];
        $userinfo->job = $params['job'];
        $userinfo->birthday = $params['birthday'];
        $userinfo->path = $params['path'];
        $userinfo->save();
       $Userinfo =  Userinfo::where('user_id',$userid)->find();

        return $Userinfo;
    }


    /* 修改密码
            * oldpassword 输入旧密码
            * $user['password'] 原先密码
            */
    public function repassword(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid = request()->userId;
        $user = $this->where('id',$userid)->find();

        // 手机注册的用户并没有原密码,直接修改即可
        if ($user['password']) {
            // 判断旧密码是否正确

            $this->checkPassword($params['oldpassword'],$user['password']);
        }
        if ( $params['newpassword'] !== $params['renewpassword']){
             TApiException(200,'输入两次密码不一致',40008);
        }

        // 修改密码


        $res = $this->where('id',$userid)->update(['password'=> md5($params['newpassword'])]);
        if (!$res) TApiException(200,'修改密码失败',20009);
        $user = $this->where('id',$userid)->find();
        // 更新缓存信息
        $token = createToken($user->toArray());
        Cache::set('Token',$token);
    }
    // 关联关注
    public function withfollow(){
        return $this->hasMany('Follow','user_id');
    }
// 关注用户
    public function ToFollow(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $user_id = request()->userId;
        $follow_id = $params['follow_id'];
        // 不能关注自己
        if($user_id == $follow_id) TApiException(200,'非法操作',10000);
        // 获取到当前用户的关注模型

        $follow = Follow::where('follow_id',$follow_id)->find();
        if($follow) TApiException(200,'已经关注过了',10000);
        Follow::create([
            'user_id'=>$user_id,
            'follow_id'=>$follow_id
        ]);
        return true;
    }

    // 取消关注
    public function ToUnFollow(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $user_id = request()->userId;
        $follow_id = $params['follow_id'];
        // 不能取消关注自己
        if($user_id == $follow_id) TApiException(200,'非法操作',10000);
        $follow = Follow::where('follow_id',$follow_id)->find();
        if(!$follow) TApiException(200,'暂未关注',10000);
        $rel =   Follow::where('follow_id',$params['follow_id'])->where('user_id',$user_id)->delete();
      if ($rel){
            return true;
        }else{
            TApiException(200,'非法操作',1001);
        }
    }

    // 获取互关列表
    public function getFriendsList(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid = request()->userId;
        //halt($userid);
        $page = $params['page'];
        $follows = Db::table('user')->where('id','IN', function($query) use($userid){
            $query->table('follow')
                ->where('user_id', 'IN', function ($query) use($userid){
                    $query->table('follow')->where('user_id', $userid)->field('follow_id');
                })->where('follow_id',$userid)
                ->field('user_id');
        })->field('id,username,userpic')->page($page,10)->select();
        return $follows;
    }

    // 关联粉丝列表
    public function fens(){
        return $this->belongsToMany('User','Follow','user_id','follow_id');
    }
// 获取当前用户粉丝列表
    public function getFensList(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid = request()->userId;
        $fens = $this->where('id',$userid)->with('fens')->page($params['page'],10)->select()->toArray();
        return $this->filterReturn($fens);
    }

// 关注和粉丝返回字段
    public function filterReturn($param = []){
        $arr = [];
        $length = count($param);
        for ($i=0; $i < $length; $i++) {
            $arr[] = [
                'id'=>$param[$i]['id'],
                'username'=>$param[$i]['username'],
                'userpic'=>$param[$i]['userpic'],
            ];
        }
        return $arr;
    }
    // 关联关注列表
    public function follows(){
        return $this->belongsToMany('User','Follow','follow_id','user_id');
    }
// 获取当前用户关注列表
    public function getFollowsList(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid = request()->userId;
        $follows = $this->where('id',$userid)->with('follows')->page($params['page'],10)->select()->toArray();
        return $this->filterReturn($follows);
    }
    // 获取当前用户关注列表
    public function getFollowPostList(){
        // 获取所有参数
        $params = request()->param();
        // 获取用户id
        $userid = request()->userId;

        $follows = Follow::where('user_id',$userid)->with(['post','user'])->page($params['page'],10)->select()->toArray();

        return $follows;
    }
    // 获取当前用户关注列表
    public function getuserinfo(){
        // 获取所有参数
        $userId = request()->param('userId');
        // 获取用户id


        $follows = User::where('id',$userId)->with(['userinfo'])->find();

        return $follows;
    }
  }