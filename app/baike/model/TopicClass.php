<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use think\Model;

class TopicClass extends Model
{
// 获取所有话题分类
    public static  function getTopicClassList(){
        return self::field('id,classname')->where('status',1)->select();
    }

    // 关联话题
    public function topic(){
        return $this->hasMany('Topic','topic_class_id','id');
    }
// 获取指定话题分类下的话题（分页）
    public function getTopic(){
        // 获取所有参数
        $param = request()->param();

        return Topic::where('topic_class_id',$param['id'])->page($param['page'],20)->select();
    }
}