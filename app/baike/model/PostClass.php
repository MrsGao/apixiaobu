<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use think\Model;

class PostClass extends Model
{

// 获取所有文章分类
    public static  function getPostClassList(){
        return self::field('id,classname')->where('status',1)->select();
    }

    // 关联文章模型
    public function post(){
        return $this->hasMany(Post::class,'post_class_id');
    }



// 关联分享
    public function share(){
        return $this->belongsTo('Post','share_id','id');
    }
    // 关联图片表
    public function images(){
        return $this->belongsToMany('Image','post_image');
    }




// 获取指定话题下的文章（分页）
    public function getPost(){
        // 获取所有参数
        $param = request()->param();
        //$param['id']
        return  Post::where('post_class_id',$param['id'])->with(['support','user'=>function($query){
            return $query->field('id,username,userpic');
        },'images'=>function($query){
            return $query->field('url');
        },'share'])->page($param['page'],10)->select();

    }


}