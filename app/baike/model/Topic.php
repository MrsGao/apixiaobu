<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use think\Model;

class Topic extends Model
{
// 获取热门话题列表
    public static function gethotlist(){
        return self::where('type',1)->limit(10)->select()->toArray();
    }

    // 关联文章
    public function post(){
        return $this->belongsToMany('Post','topic_post');
    }



// 获取指定话题下的文章（分页）
    public function getPost(){
        // 获取所有参数

        $param = request()->param();

        return Post::where('post_class_id',$param['id'])->with(['user',"images",'share'])->page($param['page'],10)->select();

    }
    // 根据标题搜索话题
    public function Search(){
        // 获取所有参数
        $param = request()->param();
        return $this->where('title','like','%'.$param['keyword'].'%')->page($param['page'],10)->select();
    }

}