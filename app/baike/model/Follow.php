<?php
// +----------------------------------------------------------------------
// | najing [ 无敌是多么寂寞 ]
// | Author: H客先生
// +----------------------------------------------------------------------


namespace app\baike\model;


use think\Model;

class Follow extends Model
{
// 关联文章
    public function post(){
        return $this->hasMany(Post::class,'id','follow_id');
    }
    // 关联文章
    public function user(){
        return $this->hasMany(User::class,'id','user_id')->field(['id','username','userpic']);
    }
}