<?php
// 事件定义文件
return [
    //事件类
    'bind'      => [
    ],
    //事件监听听
    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'swoole.task' => [
            app\listener\OrderCancelTask::class,
        ],
    ],
    //事件订阅类
    'subscribe' => [
    ],
];
