<?php


namespace app\wechat\model;


use think\facade\Db;

class Product extends BaseModel
{
    protected  $hidden = ['summary','update_time','delete_time','create_time','pivot'];

    /** 模型读取器可以在页面加参数
     * @param $value
     * @return string
     * $data  循环当前所有数据
     */
    public function getmainImgUrlAttr($value, $data)
    {
        return  $this->preGetUrlAttr($value,$data);

    }

    /**
     * 得到全部商品
     * @param $count
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */

    public static  function  getProducts($count){
     return   Product::order('id desc')->limit($count)->select()->toArray();
    }

    /*页面分类数据调用
     * @param $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getProductsByCategory($id){
        return self::where('category_id',$id)->select()->toArray();
    }

    /**
     * 图片属性
     */
    public function imgs()
    {
        return $this->hasMany('ProductImage', 'product_id', 'id');
    }
    /**
     * 商品属性
     */
    public function properties()
    {
        return $this->hasMany('ProductProperty', 'product_id', 'id');
    }

    /**
     * 获取商品详情
     * @param $id
     * @return null | \app\api\model\Product
     */
    public static function getProductDetail($id)
    {

        $product = self::with(['imgs' => function($query){
                $query->with('imgUrl');
            },'properties'])
            ->find($id)->toArray();

        return $product;
    }


}