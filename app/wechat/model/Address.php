<?php


namespace app\wechat\model;


class Address extends BaseModel
{
    public static function createUserAddress($address,$ID){
        $Address  = new  Address();
        $Address['user_id']=$ID;
       $OldAddress =  $Address->where('detail',$address['detail'])->find();
       if (empty($OldAddress)){
           $Address->save($address);
       }else{
         return  $OldAddress['id'];
       }
        return $Address->id;

    }
}