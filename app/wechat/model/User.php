<?php


namespace app\wechat\model;


use think\facade\Cache;
use think\facade\Db;

class User extends BaseModel
{

    protected $autoWriteTimestamp = 'datetime';

    /**  通过openid添加数据,判断openid是否存在
     * @param $openid
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getUser($openid,$data){
       $users = Db::name('user')->where('openid',$openid)->find();

       if ($users){
           Cache::HSet('userOpenId'.$users['id'],$users['id'],json_encode($users));
           $toekn =  createToken([
               'id'=>$users['id'],
               'userOpenId'=>$users['openid']
           ]);
           $res = [
               "token"=>$toekn,
               "openid"=>$data->openid,
               "session_key"=>$data->session_key,
               "unionid"=>$data->unionid
           ];

           return $res;
       }

    }


    public static function createUser($iphone,$openid){
       $users =  Db::name('user')->where('iphone',$iphone)->find();
       if (!$users){
           $user = new User();
           $user->openid=$openid;
           $user->iphone=$iphone;
           $user->save();
           $users =Db::name('user')->where('id',$user->id)->find();
       }

        $toekn =  createToken([
            'id'=>$users['id'],
            'userOpenId'=>$users['openid']
        ]);
        return $toekn;


    }
}