<?php


namespace app\wechat\model;


class Order extends BaseModel
{
    protected  $connection = 'mysql2';
    protected $hidden = ['user_id', 'delete_time', 'update_time'];
    public function getSnapAddressAttr($value){
        if(empty($value)){
            return null;
        }
        return json_decode(($value));
    }

    /** 数据预先处理
     * @param $value
     * @return mixed|null
     */
    public function getSnapItemsAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return json_decode($value);
    }
    public static function getSummaryByPage($page=1, $size=20,$uid){
        $pagingData = self::where('user_id',$uid)->order('create_time desc')
            ->paginate($size, true, ['page' => $page]);
        return $pagingData ;
    }
}