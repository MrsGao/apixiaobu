<?php


namespace app\wechat\model;


use app\wechat\model\Banner as BannerModel;
use think\Model;

class Banner extends BaseModel
{
    public function BannerItem()
    {
        return $this->hasMany(BannerItem::class,'banner_id','id')->field(["id","img_id","key_word","type","banner_id",]);
    }
    public static function getBanNen($Id){
      $BannerModel  =  self::with(['BannerItem','BannerItem.image'])->field(['id','name','description'])->find($Id);
        return $BannerModel;
    }

}