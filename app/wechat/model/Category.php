<?php


namespace app\wechat\model;


class Category extends BaseModel
{

    public function getmainImgUrlAttr($value, $data)
    {
        return  $this->preGetUrlAttr($value,$data);

    }

    public function Topimage(){
        return $this->hasOne(Image::class,'id','topic_img_id')->field([
            "id","url","from"]);
    }
    public function image(){
        return $this->belongsTo(Image::class,'img_id','id')->field([
            "id","url","from"]);
    }

    public static function getProducts(){
        $CategoryModel  =  self::with(['image'])->select();
        return $CategoryModel;
    }
    public static function getCategory($id){
        $CategoryModel  =  self::where('id',$id)->with('Topimage')->find();
        return $CategoryModel;
    }
}