<?php


namespace app\wechat\model;


class Theme extends BaseModel
{
    protected  $hidden = ['update_time','delete_time'];

    /** 主题图
     * @return \think\model\relation\BelongsTo
     */
    public function topicImg(){
        return $this->belongsTo('Image','topic_img_id','id');
    }

    /**专题列表页
     * @return \think\model\relation\BelongsTo
     */
    public function headImg(){
        return $this->belongsTo('Image','head_img_id','id');
    }

    public function themeProduct(){
        return $this->belongsToMany('Product','themeProduct','product_id','theme_id');
    }

}