<?php


namespace app\wechat\model;


use think\Model;

class BaseModel extends Model
{
    /** 模型读取器可以在页面加参数
     * @param $value
     * @return string
     * $data  循环当前所有数据
     */
    protected function preGetUrlAttr($value, $data)
    {
        $finalUrl = $value;
        if ($data['from'] == 1) {
            $finalUrl = config('Domain.domain') . $finalUrl;
        }
        return $finalUrl;

    }

}