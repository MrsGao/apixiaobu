<?php


namespace app\wechat\model;

use think\Model;

class Image extends BaseModel
{
    /**
     * @var string[]  返回数据处理
     */
   protected  $hidden = ['id','from','delete_time','update_time'];
    /** 模型读取器可以在页面加参数
     * @param $value
     * @return string
     * $data  循环当前所有数据
     */
    public function getUrlAttr($value, $data)
    {
        return  $this->preGetUrlAttr($value,$data);

    }

}