<?php


namespace app\wechat\model;


use think\facade\Db;
use think\Model;

class BannerItem extends BaseModel

{
    public function image(){
        return $this->belongsTo(Image::class,'img_id','id')->field([
            "id","url","from"]);
    }

}