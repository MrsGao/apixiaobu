<?php


namespace app\wechat\lib;

use \app\wechat\model\Order as OrderModeL;
use think\Log;


class Pay
{
    private $orderNo;
    private $orderID;
//    private $orderModel;

    function __construct($orderID)
    {

        if (!$orderID)
        {
            return show(0,'订单号不允许为NULL');

        }
        $this->orderID = $orderID;
    }

    public function pay($openid,$orderNo)
    {

        $this->orderNo = $orderNo;
        $this->checkOrderValid();
        $order = new Order();
        $status = $order->checkOrderStock($this->orderID);

        if (!$status['pass'])
        {
            return $status;
        }
        return $this->makeWxPreOrder($status['orderPrice'],$openid);

    }
// 构建微信支付订单信息
    private function makeWxPreOrder($totalPrice,$openid)
    {

        $wxOrderData = new \WxPayUnifiedOrder();
        //支付订单号
        $wxOrderData->SetOut_trade_no($this->orderNo);
        //支付方式
        $wxOrderData->SetTrade_type('JSAPI');
        //支付总金额
        $wxOrderData->SetTotal_fee(1);

        $wxOrderData->SetBody('零食商贩'.rand(1,1000));
        //获得openid
        $wxOrderData->SetOpenid($openid);
        //回调地址
        $wxOrderData->SetNotify_url(config('wechat.pay_back_url'));

        return $this->getPaySignature($wxOrderData);
    }
//向微信请求订单号并生成签名
    private function getPaySignature($wxOrderData)
    {
        //微信预支付订单接口
        $wxOrder = \WxPayApi::unifiedOrder($wxOrderData);

        // 失败时不会返回result_code
        if($wxOrder['return_code'] != 'SUCCESS' || $wxOrder['result_code'] !='SUCCESS'){
            \think\facade\Log::record($wxOrder,'error');
            \think\facade\Log::record('获取预支付订单失败','error');

                $res =  [
                    'code'=>0,
                    'msg' => $wxOrder
                ];


            return $res;
        }
        $this->recordPreOrder($wxOrder);
        //获取签名
        $signature = $this->sign($wxOrder);

        return $signature;
    }
    private function recordPreOrder($wxOrder){

        // 必须是update，每次用户取消支付后再次对同一订单支付，prepay_id是不同的
        OrderModeL::where('id', '=', $this->orderID)
            ->update(['prepay_id' => $wxOrder['prepay_id']]);
    }
    // 签名
    private function sign($wxOrder)
    {
        $jsApiPayData = new \WxPayJsApiPay();
        $jsApiPayData->SetAppid(config('wechat.Appid'));
        $jsApiPayData->SetTimeStamp((string)time());
        $rand = md5(time() . mt_rand(0, 1000));
        $jsApiPayData->SetNonceStr($rand);
        $jsApiPayData->SetPackage('prepay_id=' . $wxOrder['prepay_id']);
        $jsApiPayData->SetSignType('md5');
        $sign = $jsApiPayData->MakeSign();
        $rawValues = $jsApiPayData->GetValues();
        $rawValues['paySign'] = $sign;
        unset($rawValues['appId']);
        return $rawValues;
    }


    /**
     * @return bool
     * @throws OrderException
     * @throws TokenException
     */
    private function checkOrderValid()
    {
        $order = OrderModeL::where('id', '=', $this->orderID)
            ->find();
        if (!$order)
        {
            return  show(0,'订单不存在，请检查ID');
        }
//        $currentUid = Token::getCurrentUid();
        if($this->isValidOperate($order->user_id))
        {
            return show(0,'订单与用户不匹配');

        }
        if($order->status != 1){
            return show(0,'订单已支付过啦');

        }
        $this->orderNo = $order->order_no;
        return true;
    }
    /**
     * 检查操作UID是否合法
     * @param $checkedUID
     * @return bool
     * @throws Exception
     * @throws ParameterException
     */
    public static function isValidOperate($checkedUID)
    {

        if(!$checkedUID){
            return show(0,'检查UID时必须传入一个被检查的UID');

        }

        return false;
    }



}