<?php


namespace app\wechat\lib;

use app\common\controller\ToolService;
use app\wechat\model\Product;

use think\Exception;

use \app\wechat\model\Order as Order;
use think\facade\Db;
use think\facade\Log;
use \app\wechat\lib\Order as OrderLib;

require_once APP_ROOT."/WxPay/WxPay.Api.php";

class WxNotify
{
    public function NotifyProcess($data)
    {

        //支付成功回调

        if ($data['result_code'] == 'SUCCESS') {
            //获取订单号
            $orderNo = $data['out_trade_no'];

            Db::startTrans();
            try {
                //找到当前订单信息
                $order = Order::where('order_no', '=', $orderNo)->lock(true)->find();

                //如果订单没有支付
                if ($order->status == 1) {
                    //库存量检测
                    $service = new OrderLib();
                    $status = $service->checkOrderStock($order->id);

                    if ($status['pass']) {
                        $this->updateOrderStatus($order->id, true);

                        $this->reduceStock($status);

                    } else {
                        $this->updateOrderStatus($order->id, false);
                    }
                }

               Db::commit();
            } catch (Exception $ex) {
                Db::rollback();
                Log::error($ex);
                // 如果出现异常，向微信返回false，请求重新发送通知
                return false;
            }
        }

        exit(ToolService::arrayToXml($data));

    }

    /** 修改订单状态
     * @param $orderID
     * @param $success
     */
    private function updateOrderStatus($orderID, $success)
    {
        $status = $success ? config('wechat.PAID'): config('wechat.PAID_BUT_OUT_OF');

        Order::where('id', '=', $orderID)
            ->update(['status' => $status]);
    }

    private function reduceStock($status)
    {

        foreach ($status['pStatusArray'] as $singlePStatus) {
            Db::table('Product')->where('id', '=', $singlePStatus['id'])
                ->dec('stock', $singlePStatus['count']);
        }
    }

}