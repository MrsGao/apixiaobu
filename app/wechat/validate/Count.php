<?php
/**
 * Created by 七月
 * User: 七月
 * Date: 2017/2/18
 * Time: 12:35
 */
namespace app\wechat\validate;

class Count extends Basevalidate
{
    protected $rule = [
        'count' => 'isPositiveInteger|between:1,15',
    ];

    protected $message  =   [
        'count.between'  => '年龄只能在1-120之间',
    ];
}
