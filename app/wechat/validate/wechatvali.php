<?php


namespace app\wechat\validate;


class wechatvali extends Basevalidate
{
    protected $rule =   [
        'name'  => 'require|max:6',
        'age'   => 'number|between:1,120',

    ];

    protected $message  =   [
        'name.require' => '名称必须',
        'name.max'     => '名称最多不能超过6个字符',
        'age.number'   => '年龄必须是数字',
        'age.between'  => '年龄只能在1-120之间',

    ];

    protected $scene = [
        'edit'  =>  ['name','age'],
    ];
}