<?php


namespace app\wechat\model;


use app\wechat\validate\Basevalidate;

class User extends Basevalidate
{
    protected $rule = [
        'code' => 'isNotEmpty|require',
    ];

    protected $message  =   [
        'count.require'  => 'code不能为空',
    ];
}