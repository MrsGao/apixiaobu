<?php


namespace app\wechat\validate;



use app\lib\exception\ParameterException;

class OrderPlace extends Basevalidate
{
    /** 对苏数组类型数据进行校验
     * @var string[]
     *
     * 校验数据
     *   protected $value =[
    [
    'product_id'=>1 ,
    'count' =>3,
    ],
    [
    'product_id'=>1 ,
    'count' =>3,
    ],
    [
    'product_id'=>1 ,
    'count' =>3,
    ],

    ];
     */
    //验证规则 验证器下面只有 rule 可以自动验证
    protected $rule = [
        'products' => 'checkProducts'
    ];

    //定义一个自定义验证规则 ，所以需要手动调用验证
    protected $singleRule = [
        'product_id' => 'require|isPositiveInteger',
        'count' => 'require|isPositiveInteger',
    ];

    protected function checkProducts($values)
    {
        if(empty($values)){
            return  show(0,'商品列表不能为空');

        }
        //循环遍历每一个子元素
        foreach ($values as $value)
        {
            $this->checkProduct($value);
        }
        return true;
    }

    private function checkProduct($value)
    {
        $validate = new Basevalidate($this->singleRule);
        $result = $validate->check($value);
        if(!$result){
          return  show(0,'商品列表参数错误');

        }
    }
}