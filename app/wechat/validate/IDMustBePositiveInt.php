<?php
/**
 * Created by 七月
 * User: 七月
 * Date: 2017/2/18
 * Time: 12:35
 */
namespace app\wechat\validate;

class IDMustBePositiveInt extends Basevalidate
{

    protected $rule = [
        'id' => 'require|isPositiveInteger',
    ];
}
