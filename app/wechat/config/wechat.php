<?php
return [
    //小程序appid
    'Appid'=>'wxfa330c53097b34ab',
    //小程序secret
    'secret'=>'55a5b98f17c31303114129c8a2297329',
    //商户id
    'shopId'=>'1565888351',
    //商户key
    "key"=>'e10adc3949ba59abbe56e057f20f883e',
    // 待支付
    'UNPAID'=>1,
    // 已支付
    'PAID'=>2,
    // 已发货

    'DELIVERED'=>3,

    // 已支付，但库存不足
    'PAID_BUT_OUT_OF'=>4,
    // 已处理PAID_BUT_OUT_OF
    'HANDLED_OUT_OF'=>5,
    //微信支付回调路由地址
    'pay_back_url'=>'https://zerg.budaohuaxia.com/wechat/pay/notify'
];