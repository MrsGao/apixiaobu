<?php


namespace app\wechat\controller;


use app\api\business\Token;
use app\BaseController;
use app\common\controller\Wechat;

use app\common\controller\WXBizDataCrypt;
use app\common\controller\WXBizDataCrypts;
use app\Request;
use app\wechat\model\User as UserModel;
use think\facade\Cache;
use think\facade\Db;

class LoginToken extends BaseController
{
    /** 换取openid
     * @param Request $request
     * @param string $code
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */

    public function getToken(Request $request, $code = '')
    {
        if ($request->isPost()) {

            //获取微信code
            $result = json_decode(Wechat::wxLogin($code));
            //获取token
            $toekn = UserModel::getUser($result->openid, $result);
            if ($toekn) {
                return show(1, 'success', $toekn);
            } else {
                return show(0, '您是新用户', $result);
            }

        }
    }

    /** 微信拉取手机号进行注册
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getNumber(Request $request)
    {
       $token =  Wechat::getPhoneNumber($request->param());

      return show(1,'请求成功',['token'=>$token]);
    }

    /** 检测token 是否存在
     * @param string $token
     * @return array|\think\response\Json
     */
    public function verifyToken($token='')
    {
        if(!$token){
            return  show(0,'token不允许为空');

        }

        return json(authToken($token));

    }

    //注册新用户
    public function CreateUser(Request  $request){
        $data = $request->param();
        halt($data);
    }

}