<?php


namespace app\wechat\controller;


use app\BaseController;
use \app\wechat\model\Theme as ThemeModel;

class Theme extends BaseController
{
    /** banner 数据返回
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getSimpleList()
    {
       $ThemeModels = ThemeModel::with(['topicImg','headImg'])->select()->toArray();
       return  show(1,'请求成功',$ThemeModels);
    }
    public function getSimpProdoct()
    {
        $ThemeModels = ThemeModel::with('themeProduct')->select()->toArray();
        return  show(1,'请求成功',$ThemeModels);
    }

    public function getSimpleLists($id='')
    {

        $ThemeModels = ThemeModel::where('id',$id)->with(['topicImg','headImg','themeProduct'])->find()->toArray();
        return  show(1,'请求成功',$ThemeModels);
    }
}