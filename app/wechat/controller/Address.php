<?php


namespace app\wechat\controller;

use app\BaseController;
use \app\wechat\model\Address as AddressModel;
use app\wechat\validate\AddressNew;
use think\Request;


class Address extends BaseController

{

    /** 个人地址创建功能
     * @param Request $request
     * @return \think\response\Json
     */
    public function createOrUpdateAddress(Request $request){

        if ($request->isPost()){

            $uid =  authToken($request->header('token'));

           $AddressNew = new  AddressNew();
           $AddressNew->goCheck();
            $params = $AddressNew->getDataByRule($request->param());

           if ($uid['code'] == 1){
              $res =  AddressModel::createUserAddress($params,$uid['user_id']);
              if ($res){
                return  show(1,'Success',$res);
              }else{
                return  show(0,'添加失败');
              }
           }

        }else{
         return  show(0,'非法请求');
        }
    }
}