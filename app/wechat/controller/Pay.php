<?php


namespace app\wechat\controller;

use app\BaseController;
use app\common\controller\ToolService;
use app\Request;
use \app\wechat\lib\Pay as PayLib;
use app\wechat\lib\WxNotify;
use  app\wechat\model\Order as OrderModel;
require_once APP_ROOT."/WxPay/WxPay.Config.php";
require_once APP_ROOT."/WxPay/WxPay.Api.php";


class Pay extends BaseController
{
    /** 下单接口
     * @param Request $request
     * @return \think\response\Json
     */
    public function getPreOrder(Request  $request)
    {
        $data = $request->param();
        $OreDer = OrderModel::where('id',$data['id'])->find();
        $pay= new PayLib($data['id']);

        return  show(1,'返回成功',$pay->pay($data['openid'],$OreDer['order_no']));




    }

    /**
     * 微信通知
     */
    public function redirectNotify()
    {
        //接受回调通知

        $data = file_get_contents('php://input');

        $data = ToolService::xmlToArray($data);

        $notify = new WxNotify();

        //不能调用NotifyProcess 需要调用handle

        $notify->NotifyProcess($data);

    }

    /**
     * 测试notify 微信回调
     */
    public function receiveNotify()
    {
//        $xmlData = file_get_contents('php://input');
//        Log::error($xmlData);
        $notify = new WxNotify();
        $notify->handle();
//        $xmlData = file_get_contents('php://input');
//        $result = curl_post_raw('http:/zerg.cn/api/v1/pay/re_notify?XDEBUG_SESSION_START=13133',
//            $xmlData);
//        return $result;
//        Log::error($xmlData);
    }


}