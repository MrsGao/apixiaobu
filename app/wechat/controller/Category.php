<?php


namespace app\wechat\controller;


use app\BaseController;
use app\wechat\model\Category as CategoryModel;

class Category extends BaseController
{
    public function getCategory()
    {
        $getProducts =  CategoryModel::getProducts();
        return show(1,'返回成功',$getProducts);
    }
}