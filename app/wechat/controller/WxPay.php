<?php


namespace app\wechat\controller;



use app\BaseController;

class WxPay extends BaseController
{
    public function getPreOrder($id='')
    {

        $pay= new PayService($id);
        return $pay->pay();
    }
}