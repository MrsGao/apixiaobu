<?php


namespace app\wechat\controller;



use \app\wechat\validate\IDMustBePositiveInt;
use app\BaseController;
use app\lib\exception\ProductException;
use \app\wechat\model\Category as CategoryModel;
use \app\wechat\model\Product as ProductModel;
use app\wechat\validate\Count;

class Product extends BaseController
{
    public function getRecent($conut=5){

        (new Count())->goCheck();
      $getProducts =  ProductModel::getProducts($conut);
      return show(1,'返回成功',$getProducts);

    }
    public function getAllInCategory($id){

       $products = ProductModel::getProductsByCategory($id);
        $Category = CategoryModel::getCategory($id);
        $products['category'] =  $Category;

        return show(1,'',$products);
    }

    /**
     * 获取商品详情
     * 如果商品详情信息很多，需要考虑分多个接口分布加载
     * @url /product/:id
     * @param int $id 商品id号
     * @return \app\api\controller\v1\Product
     * @throws ProductException
     */
    public function getOne($id)
    {
        (new IDMustBePositiveInt())->goCheck();
        $product = ProductModel::getProductDetail($id);
         return  show(1,'Success',$product);

    }




}