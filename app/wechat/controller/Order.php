<?php

/**
 *  订单详情表
 */
namespace app\wechat\controller;




use app\BaseController;
use app\Request;
use app\wechat\validate\OrderPlace;
use app\wechat\lib\Order as OrderService;
use \app\wechat\model\Order as OrderModel;
class Order extends BaseController
{
    /**
     *  下单接口
     */
    public function placeOrder(Request $request)
    {

        if ($request->isPost()){

            (new OrderPlace())->goCheck();

            $data= $request->param();

            $uid =  authToken($request->header('token'));

            $order = new OrderService();
            unset($data['token']);
            $status = $order->place($uid['user_id'], $data['products']);
            return  show(1,'返回成功',$status);
        }

    }

    /**
     * 获取全部订单简要信息（分页）
     * @param int $page
     * @param int $size
     * @return array
     * @throws \app\lib\exception\ParameterException
     */
    public function getSummary($page=1, $size = 20,Request $request){

        if ($request->isPost()) {
            $uid = $request->header('token');
            $uid = authToken($uid);

            $pagingOrders = OrderModel::getSummaryByPage($page, $size, $uid['user_id']);

            if ($pagingOrders->isEmpty()) {
                $datas = [
                    'current_page' => $pagingOrders->currentPage(),
                    'data' => []
                ];
                return show(1, '返回成功', $datas);

            }
            $data = $pagingOrders->hidden(['snap_items', 'snap_address'])
                ->toArray();

            $datas = [
                'current_page' => $pagingOrders->currentPage(),
                'data' => $data
            ];


            return show(1, '返回成功', $datas);
        }
    }

    /**
     * 获取订单详情
     * @param $id
     * @return static
     * @throws OrderException
     * @throws \app\lib\exception\ParameterException
     */
    public function getDetail(Request $request)
    {
        $id= $request->param('id');

        $orderDetail = OrderModel::where('id',$id)->find();

        if (!$orderDetail)
        {
            return  show(0,'订单不存在，请检查ID');
        }
        return  show(1,'返回成功',$orderDetail->hidden(['prepay_id']));

    }

}