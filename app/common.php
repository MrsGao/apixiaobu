<?php
// 应用公共文件
use app\baike\lib\exception\BaseException;
use think\facade\Cache;

/**
 * @param $code  返回状态吗
 * @param string $message 返回错误信息
 * @param array $data 返回数据
 * @param int $httpStatus 返回header 状态
 * @return \think\response\Json
 */
function show($code,$message='error',$data=[],$httpStatus=200){

    $results =[
        'code'=>$code,
        'message'=>$message,
        'result'=>$data,
    ];
    return json($results,$httpStatus);

}

/**
 * @param string $userId
 * @return mixed
 */
 function createToken($token)
{
    $key = md5('zq8876!@!'); //jwt的签发密钥，验证token的时候需要用到
    $time = time(); //签发时间
    $expire = $time + 1000000; //过期时间 单位为妙
        $token["aud"] = "MrsGao"; //签发作者
        $token ["iat"] = $time;
        $token["nbf"]  = $time;
        $token["exp"] = $expire;
        $jwt = \Firebase\JWT\JWT::encode($token, $key);
        return $jwt;

}

//校验jwt权限API
/*
 *
 */
 function authToken($jwt = '')
{
    $key = md5('zq8876!@!');
    try {
        $jwtAuth = json_encode(Firebase\JWT\JWT::decode($jwt, $key, array('HS256')));

        $authInfo = json_decode($jwtAuth, true);

        $msg = [];

        if (!empty($authInfo['id'])) {

            $msg = [
                'code'=>1,
                'user' => $authInfo,
                'msg' => "Token验证通过"
            ];
        } else {
            //dump(2);
            $msg = [
                'code'=>0,
                'errorCode' => 1002,
                'msg' => "Token验证不通过,用户不存在"
            ];
        }
        return $msg;
    }  catch (\Firebase\JWT\ExpiredException $e) {
        echo json_encode([
            'code'=>0,
            'errorCode' => 1003,
            'msg' => 'Token过期'
        ]);
        exit;
    } catch (\Exception $e) {
        echo json_encode([
            'code'=>0,
            'errorCode' => 1002,
            'msg' => 'Token无效'
        ]);
        exit;
    }
}

/**
 * @param string $url get请求地址
 * @param int $httpCode 返回状态码
 * @return mixed
 */
function curl_get($url, &$httpCode = 0)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    //不做证书校验,部署在linux环境下请改为true
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    $file_contents = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $file_contents;
}

/**
 * @param string $url post请求地址
 * @param array $params
 * @return mixed
 */
function curl_post($url, array $params = array())
{
    $data_string = json_encode($params);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt(
        $ch, CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json'
        )
    );
    $data = curl_exec($ch);
    curl_close($ch);
    return ($data);
}

function curl_post_raw($url, $rawData)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $rawData);
    curl_setopt(
        $ch, CURLOPT_HTTPHEADER,
        array(
            'Content-Type: text'
        )
    );
    $data = curl_exec($ch);
    curl_close($ch);
    return ($data);

}

//返回无数据格式
function showResCodeWithOutDate($msb= '未知',$code=200){
    $results =[
        'code'=>$code,
        'message'=>$msb,

    ];
    return json($results);
}

//返回异常类信息封装格式
/**
 * @param int $code 返回成功
 * @param string $msg 返回信息
 * @param int $errorCode 错误代码
 * @throws BaseException
 */
function TApiException($code = 400,$msg= '异常',$errorCode=999){
    throw new BaseException(['code'=>$code,'message'=>$msg,'errorCode'=>$errorCode]);
}

// 获取文件完整url
function getFileUrl($url='')
{
    if (!$url) return;
    return url($url,'',false,true);
}

