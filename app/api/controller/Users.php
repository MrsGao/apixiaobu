<?php


namespace app\api\controller;


use app\api\business\Token;
use app\BaseController;
use app\Request;

class Users extends BaseController
{
    public function index(){
        $data = [
            'iphone'=>'15163640385',
            'nicheng'=>'gaobin',
        ];
        $token =  createToken($data);

      dump($token);
    }

    public function jiexi(Request  $request)
    {

        $token = $request->param('token');
        //验证token
        $token= authToken($token);
        return $token ;


        //return $rel;
    }

    //前端获取数据
    public function Login(Request  $request){

        $code  = $request->param("codenumber");
        $key  = $request->param('svgKey');
        $userName  = $request->param('userName');
        $password  = $request->param('password');
       $res =  \edward\captcha\facade\CaptchaApi::check($code,$key);
       if ($res){
           $users =  \app\common\model\Users::getAdminLogin($userName,$password);
           return  show(config('result.success'),'登录成功',$users);
       }else{
           return show(config('result.error'),'验证码错误,请重新输入');
       }

    }

    //获取token
    public function TokenAuth(Request $request){
        if ($request->isPost()){
        $token = $request->param('token');
        //验证token
        $token= authToken($token);
        if ($token['code'] == 3){
            return show(config('result.error'),'Token过期');
        }
        if ($token['code'] == 4){
                return show(config('result.error'),'Token失效');
            }
        if ($token['code'] == 0){
                return show(config('result.error'),'Token验证不通过,用户不存在');
            }
        return show(config('result.success'),'验证成功');
    }
    }
}