<?php


namespace app\api\controller;


use app\api\business\SmsBus;
use app\api\validate\Users;
use app\BaseController;
use app\Request;
use think\exception\ValidateException;

class Sms extends BaseController
{
     public function code(Request  $request){

         $phobeNumber = $request->param('iphone_number','','trim');
         $data= [
             'iphone_number'=>$phobeNumber,
         ];
         try {
             //调用验证机制方法
            Validate(Users::class)->scene('send_code')->check($data);
         }catch (ValidateException $e){
             return show(config('status.error'),$e->getError());
         }
        //短信发送
         if (SmsBus::sendCode($phobeNumber,4,'Ali')){

            return show(config('status.success'),'验证码发送成功');
         }else{
             return show(config('status.error'),'验证码发送失败');
         }
     }
}