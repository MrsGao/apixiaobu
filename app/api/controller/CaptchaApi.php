<?php


namespace app\api\controller;


use app\BaseController;
use app\Request;
use think\response\Json;

class CaptchaApi extends BaseController
{
    public function CaptchaApi(Request $request){
        if ($request->isGet()){
            $data = \edward\captcha\facade\CaptchaApi::create('verify');
            return \json($data);
        }

    }
}