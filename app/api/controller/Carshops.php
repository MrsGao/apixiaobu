<?php


namespace app\api\controller;


use app\BaseController;
use app\Request;

class Carshops extends BaseController
{
    /** 请求单张洗车卡
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function carWashCard(Request $request){
        if ($request->isGet()){
           $res = \app\common\model\Carshop::getCard(0,1);
           return show(1,'请求成功',$res);
        }

    }
}