<?php


namespace app\api\controller;


use app\admin\model\Carshop;
use app\api\business\Token;
use app\BaseController;
use app\common\lib\ExportExcel;
use app\common\model\Sharingmd;
use app\Request;
use think\facade\Db;

class Sharingmds extends BaseController
{
    /**
     * @param Request $request
     * @return \think\response\Json
     * 前端拉取未审核数据
     */
    public function SharingNotLists(Request  $request){
        $token = $request->param('token');
        $token= authToken($token);

        if ($token['code']  == 1){
            return show(1,'请求成功',Sharingmd::SharingNotLists());

        }
    }

    /** 门店删除功能
     * @param Request $request
     */
    public function DelSharing(Request  $request){

        if ($request->isGet()){
        $id = $request->param('id');

        if ($id){
            $token = $request->param('token');
            $token= authToken($token);

            if ($token['code']  == 1){
                return show(1,'请求成功',Sharingmd::DelSharing($id));
            }
        }
        }else{
            show(0,'请求方式不正确');
        }

    }

    /**导出表格数据
     * @param Request $request
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function SharingmdsExcel(Request  $request){

        $token = $request->param('token');

        $token= authToken($token);

        if ($token['code']  == 1){
          $ExportExcel=  new ExportExcel();
          $ExportExcel->excel(Sharingmd::SharingNotLists());
        }
    }
    /*
     *  拉出单条数据
     */
    public function SharingmdsFinds(Request  $request){

        $token = $request->param('token');

        $token= authToken($token);

        if ($token['code']  == 1){
            $id=  $request->param('id');
            $Sharingmd =  Sharingmd::SharingOne($id);
          return show(1,'请求成功',$Sharingmd);
        }
    }

    /**men 门店审核
     * @param Request $request
     */
    public function StoreAudit(Request  $request){
        if ($request->isPost()){
            $data = $request->param();
            //验证token
            $token= authToken($data['token']);
            if ($token['code']  == 1){
             $res =    Sharingmd::updateFindSharing($data['id'],$data);
             if ($res){
                 Sharingmd::UpdateCards($data['value'],$data['id']);
                 return  show(1,'请求成功','门店审核成功');
             }
            }

        }

    }

    //
    public function SharingNotListsPage(Request  $request){
        $data = $request->param();

        $token = $request->param('token');
        $token= authToken($token);

        if ($token['code']  == 1){
            /**
             *  status  状态审核  0没通过 1已通过
             * types  2"实体店   1：线上
             */
            $ReturnData = Sharingmd::SharingNotListsPage($data['page'],$data['pageSize'],1,2);
            return show(1,$ReturnData['count'],$ReturnData['Sharingmds']);

        }
    }

    /** 前端swith 更新状态
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function SwithUpdate(Request  $request){

        if ($request->isGet()){
            // id  数据id   num  数据切换值   Db 要修改数据表字段名
            $id = $request->param('id');
            $num = $request->param('num');
            $Db = $request->param('Db');

            if ($id){
                $token = $request->param('token');
                $token= authToken($token);
                $num = !$num;
                if ($token['code']  == 1){
                     return show(1,'请求成功',Sharingmd::SwitchUpdates($id,$num,$Db));

                }

            }
        }else{
            show(0,'请求方式不正确');
        }

    }

    /*
     * 提取字符串首字母
     */



}