<?php


namespace app\api\validate;


use think\Validate;

class Users extends Validate
{
    protected  $rule = [
        'username'=>'require',
        'iphone_number'=>'require',

    ];

    protected $message = [
        'username.require'=>'用户名必须输入',
        'iphone_number.require'=>'电话必须输入',
    ];

    //应用场景
    protected  $scene = [
      'send_code'=>['iphone_number']
    ];

}