<?php

use think\facade\Route;


Route::get('show', 'index/index')->middleware(app\api\middleware\check::class);
Route::rule('shows', 'Show/index','GET')->middleware(app\api\middleware\apis::class);
