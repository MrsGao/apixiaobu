<?php


namespace app\api\business;
/**
 * Class Token token验证
 * @package app\api\business
 */


class Token
{
    /**
     * token 生成
     * @param $data
     * @return string
     */
    public static function createToken($data){
        $token=   \thans\jwt\facade\JWTAuth::builder(
            $data);
        return $token;
    }

    /**
     * token 进行验证
     * @param $token
     * @return array
     */
    public static function authToken($token){
          \thans\jwt\facade\JWTAuth::auth($token);
    }

    /** token取值
     * @param $token
     * @param $name
     * @return mixed
     */

    public static function valueToken($token,$name){
        $rel =  \thans\jwt\facade\JWTAuth::auth($token);

        return  $rel[$name]->getvalue();
    }

    /** 刷新token 原token 拉入黑名单，新token生成
     * @param $token
     * @return string
     */
    public static function reFreshToken($token){
       return \thans\jwt\facade\JWTAuth::refresh($token);
    }



}