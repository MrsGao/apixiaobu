<?php

declare(strict_types=1);
namespace app\api\business;


use app\common\lib\ClassAtr;
use app\common\lib\Num;
use app\common\lib\sms\AliSms;


class SmsBus
{
    //获取手机号
    public static function sendCode($iphone,$len,$type) :bool{
        $code = Num::getCode($len);
        /**
         * 第一种方式使用接口
        * 接口集成,实现工厂模式
        $class =  "app\common\lib\sms\\".$type.'Sms';
         $sms =  $class::sendCode($iphone,$code);*/
        /**
         * 第二种使用高大上的反射机制
         */
       $classStats = ClassAtr::smsClassStat();

       $classSobj = ClassAtr::initClass($type,$classStats,[],false);

       $sms = $classSobj::sendCode($iphone,$code);


        if ($sms){
            //短信存储redis时间为一分钟
            \cache(config('redis.code_pre').$iphone,$code,config('redis.code_expire'));

            return true;
        }
    }
}