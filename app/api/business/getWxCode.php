<?php

namespace app\api\business;
/**
 * Created by PhpStorm.
 * User: king
 * Date: 2017/9/15
 * Time: 17:58
 */
class getWxCode
{
    public function index()
    {
        return 'this is api test?';
    }

    /* 生成微信小程序的二维码（三种接口生成方法）：
    * 官方文档参数：https://mp.weixin.qq.com/debug/wxadoc/dev/api/qrcode.html
    */
    public function getwxaqrcode()
    {
        $access_token = $this->AccessToken();
        //接口A：
        //$url = 'https://api.weixin.qq.com/wxa/getwxacode?access_token='.$access_token;
        //$path="pages/mine/mine/mine?query=1";
        //$width=430;
        //$data='{"path":"'.$path.'","width":'.$width.'}';

        //接口B：
        $url='https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token='.$access_token;
        $scene='shopid=gd123&cm=comma&hi=abc';
        $width=430;
        $auto_color='false'; //默认值是false，自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调；
        $line_color='{"r":"0","g":"0","b":"0"}'; //auth_color 为 false 时生效，使用 rgb 设置颜色 例如 {"r":"xxx","g":"xxx","b":"xxx"}
        $data='{"scene":"'.$scene.'","width":'.$width.',"auto_color":'.$auto_color.',"line_color":'.$line_color.'}';

        //接口C：
        //$url = 'https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token='.$access_token;
        //$path="pages/mine/mine/mine?query=1";
        //$width=430;
        //$data='{"path":"'.$path.'","width":'.$width.'}';

        $return = $this->request_post($url,$data);
		
		return var_dump($return);
        //将生成的小程序码存入相应文件夹下
        $erweima='./wxyido/img/'.$this->getMillisecond().'.jpg';
        file_put_contents($erweima,$return);
        return '<img src="'.$erweima.'">';
    }

    //获取时间戳到毫秒级：
    public function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }

    //获取小程序的access_token：
    public function AccessToken()
    {
		//注册小程序申请的appid、key
        $appid     = config('redis.appid');
        $appsecret = config('redis.appsecret');
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";
        $AccessToken = $this->request_post($url);
        $AccessToken = json_decode($AccessToken , true);
        $AccessToken = $AccessToken['access_token'];
        return $AccessToken;
    }


    public function request_post($url,$data){
        $ch = curl_init();
        $header = "Accept-Charset: utf-8";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $tmpInfo = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
        }else{
            return $tmpInfo;
        }
    }

}

$test=new wxinfo();
echo $test->getwxaqrcode();

?>